package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.service.validators.Validator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceMesaje extends Service<Long, Message> {

    private Repository<Tuple<Long,Long>, Prietenie> repoPrietenii;
    private Repository<Long,Utilizator> repoUtilizatori;
    private Repository<Long, CererePrietenie> repoCereriPrietnii;
    private Repository<Long, Event> repoEvents;



    public ServiceMesaje(Repository<Long, Message> repoMesaje, Validator<Message> validator, Repository<Tuple<Long,Long>,Prietenie> repoPrietenii, Repository<Long,Utilizator> repoUtilizatori, Repository<Long, CererePrietenie> repoCereriPrietenii, Repository<Long, Event> repoEvents) {
        super(repoMesaje, validator);
        this.repoPrietenii = repoPrietenii;
        this.repoUtilizatori = repoUtilizatori;
        this.repoCereriPrietnii = repoCereriPrietenii;
        this.repoEvents = repoEvents;
    }


    public ServiceMesaje(Repository<Long, Message> repo, Validator<Message> validator) {
        super(repo, validator);
    }

    public List<Message> getSortedMesaje(Utilizator u1, Utilizator u2){
        return this.repo.findAll().stream()
                .filter(msg->(msg.getFrom().equals(u1.getId()) && msg.getTo().contains(u2.getId())) || (msg.getFrom().equals(u2.getId()) && msg.getTo().contains(u1.getId())))
                .sorted((msg1,msg2)->{
                    if(msg1.getDate().equals(msg2.getDate())){
                        return msg1.getId().compareTo(msg2.getId());
                    }
                    return msg1.getDate().compareTo(msg2.getDate());
                })
                .collect(Collectors.toList());
    }

    public void deleteMesajeUtilizator(Utilizator utilizator){
        List<Message> lM = new ArrayList<>();
        this.getAll().forEach(mesaj->{
            if(utilizator.getId().equals(mesaj.getFrom())){
                lM.add(mesaj);
            }
            else if(mesaj.getTo().size() == 1 && mesaj.getTo().get(0).equals(utilizator.getId())){
                lM.add(mesaj);
            }
            else{
                List<Long> lMI = new ArrayList<>();
                mesaj.getTo().forEach(idUtilizator->{
                    if(utilizator.getId().equals(idUtilizator)){
                        lMI.add(idUtilizator);
                        if(this.repo.getClass().getName().contains("MesajeDbRepository")) {
                            try {
                                Method m = this.repo.getClass().getMethod("deleteMesajToUtilizator", Long.class, Long.class);
                                m.invoke(this.repo, mesaj.getId(), idUtilizator);
                            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                lMI.forEach(idUtilizator->{
                    mesaj.getTo().remove(idUtilizator);
                });
            }
        });
        lM.forEach(mesaj->{
            this.repo.delete(mesaj.getId());
        });
    }
}
