package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.service.validators.Validator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ServiceUtilizatori extends Service<Long, Utilizator> {

    private Repository<Tuple<Long,Long>, Prietenie> repoPrietenii;
    private Repository<Long,Message> repoMesaje;
    private Repository<Long,CererePrietenie> repoCereriPrietnii;
    private Repository<Long, Event> repoEvents;

    public ServiceUtilizatori(Repository<Long, Utilizator> repoUtilizatori, Validator<Utilizator> validator, Repository<Tuple<Long,Long>,Prietenie> repoPrietenii, Repository<Long, Message> repoMesaje, Repository<Long, CererePrietenie> repoCereriPrietenii, Repository<Long, Event> repoEvents) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        super(repoUtilizatori, validator);
        this.repoPrietenii = repoPrietenii;
        this.repoMesaje = repoMesaje;
        this.repoCereriPrietnii = repoCereriPrietenii;
        this.repoEvents = repoEvents;
    }

    public void updateUsers() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if(this.repo.getClass().getName().contains("UtilizatorDbRepository")) {
            if (this.repoPrietenii.getClass().getName().contains("PrieteniiDbRepository")) {
                Method m = this.repoPrietenii.getClass().getMethod("getIdUsers");
                Set<Long> idUSers = (Set<Long>) m.invoke(this.repoPrietenii);
                idUSers.forEach(idUser -> {
                    if (!this.repo.findAll().contains(this.repo.findOne(idUser))) {
                        try {
                            Method m2 = this.repo.getClass().getMethod("getUtilizatorFromDb",Long.class);
                            Utilizator u = (Utilizator) m2.invoke(this.repo,idUser);
                            Method m3 = this.repo.getClass().getMethod("addToMemory",Utilizator.class);
                            m3.invoke(this.repo,u);
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            if (this.repoMesaje.getClass().getName().contains("MesajeDbRepository")) {
                Method m = this.repoMesaje.getClass().getMethod("getIdUsers");
                Set<Long> idUSers = (Set<Long>) m.invoke(this.repoMesaje);
                idUSers.forEach(idUser -> {
                    if (!this.repo.findAll().contains(this.repo.findOne(idUser))) {
                        try {
                            Method m2 = this.repo.getClass().getMethod("getUtilizatorFromDb",Long.class);
                            Utilizator u = (Utilizator) m2.invoke(this.repo,idUser);
                            Method m3 = this.repo.getClass().getDeclaredMethod("addToMemory",Utilizator.class);
                            m3.invoke(this.repo,u);
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            if (this.repoEvents.getClass().getName().contains("EventsDbRepository")) {
                Method m = this.repoEvents.getClass().getMethod("getIdUsers");
                Set<Long> idUSers = (Set<Long>) m.invoke(this.repoEvents);
                idUSers.forEach(idUser -> {
                    if (!this.repo.findAll().contains(this.repo.findOne(idUser))) {
                        try {
                            Method m2 = this.repo.getClass().getMethod("getUtilizatorFromDb",Long.class);
                            Utilizator u = (Utilizator) m2.invoke(this.repo,idUser);
                            Method m3 = this.repo.getClass().getDeclaredMethod("addToMemory",Utilizator.class);
                            m3.invoke(this.repo,u);
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            if (this.repoCereriPrietnii.getClass().getName().contains("CererePrietenieDbRepository")) {
                Method m = this.repoCereriPrietnii.getClass().getMethod("getIdUsers");
                Set<Long> idUSers = (Set<Long>) m.invoke(this.repoCereriPrietnii);
                idUSers.forEach(idUser -> {
                    if (!this.repo.findAll().contains(this.repo.findOne(idUser))) {
                        try {
                            Method m2 = this.repo.getClass().getMethod("getUtilizatorFromDb",Long.class);
                            Utilizator u = (Utilizator) m2.invoke(this.repo,idUser);
                            Method m3 = this.repo.getClass().getDeclaredMethod("addToMemory",Utilizator.class);
                            m3.invoke(this.repo,u);
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    private void updateFriendList(Utilizator user){
        this.getAll().forEach(utilizator -> {
            utilizator.removePrieten(user);
        });
    }

    public void updateCereriInit(){
        this.repoCereriPrietnii.findAll().forEach(cererePrietenie -> {
            Utilizator utilizator = this.getEntity(cererePrietenie.getTo());
            if(!utilizator.getCereri().contains(cererePrietenie)) {
                utilizator.addCerereDePrietenie(cererePrietenie);
            }
        });
    }

    public void updateEvents(){
        this.repoEvents.findAll().forEach(event -> {
            event.getUsers().forEach(idUser->{
                Utilizator u = this.getEntity(idUser);
                if(!u.getEvents().contains(event)) {
                    u.addEvent(event);
                }
                if(!u.getNotifyEvents().contains(event)) {
                    u.addInterestedEvent(event);
                }
            });
        });
    }

   public Utilizator getUtilizator(String username, String password) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
       if(this.repo.getClass().getName().contains("UtilizatorDbRepository")){
           Method m = this.repo.getClass().getMethod("getUtilizatorFromDb",Integer.class);
           return (Utilizator) m.invoke(this.repo,Objects.hash(username,password));
       }
       return null;
   }

    @Override
    public Utilizator updateEntity(Utilizator utilizator) {
        this.validator.validate(utilizator);
        utilizator.setFriends(this.getEntity(utilizator.getId()).getFriends());
        return super.updateEntity(utilizator);
    }

    @Override
    public Utilizator deleteEntity(Utilizator entity) {
        this.updateFriendList(entity);
        this.updateCereriStergereUtilizator(entity);
        return this.repo.delete(entity.getId());
    }

    public void addPrieten(Utilizator u1, Utilizator u2){
        this.repo.findOne(u1.getId()).addPrieten(u2);
        this.repo.findOne(u2.getId()).addPrieten(u1);
    }

    public void updateFriendList(){
        this.repoPrietenii.findAll().forEach(prietenie -> {
            Utilizator u1 = this.repo.findOne(prietenie.getId().getLeft());
            Utilizator u2 = this.repo.findOne(prietenie.getId().getRight());
            if(!u1.getFriends().contains(u2.getId()) && !u2.getFriends().contains(u1.getId())) {
                this.addPrieten(u1, u2);
            }
        });
    }

    private void updateCereriStergereUtilizator(Utilizator utilizator){
        this.repoCereriPrietnii.findAll().forEach(cererePrietenie -> {
            if(cererePrietenie.getFrom().equals(utilizator.getId())){
                Utilizator u = this.repo.findOne(cererePrietenie.getTo());
                u.removeCerere(cererePrietenie);
            }
        });
    }

    private void setAllUsersUnvisited(){
        this.getAll().forEach(utilizator -> {
            utilizator.setVisited(false);
        });
    }

    private void DFSUtil(Long id, List<List<Long>> rez, AtomicReference<Integer> idx){
        Utilizator user=this.getEntity(id);
        user.setVisited(true);
        rez.get(idx.get()).add(id);
        user.getFriends().forEach(idUser->{
            Utilizator u = this.getEntity(idUser);
            if(!u.isVisited()){
                DFSUtil(u.getId(), rez, idx);
            }
        });
    }

    public List<List<Long>> findCommunities(){
        List<List<Long>> rez = new ArrayList<>();
        AtomicReference<Integer> idx = new AtomicReference<>(0);
        this.setAllUsersUnvisited();
        this.getAll().forEach(utilizator -> {
            if(!utilizator.isVisited()) {
                List<Long> lst = new ArrayList<>();
                rez.add(lst);
                DFSUtil(utilizator.getId(), rez, idx);
                idx.set(idx.get() + 1);
            }
        });
        return rez;
    }

    public List<Long> findMostSociableCommunity(){
        AtomicReference<Integer> maxSum= new AtomicReference<>(0);
        List<List<Long>> comm=this.findCommunities();
        AtomicReference<List<Long>> rez= new AtomicReference<>(null);
        comm.forEach(community ->{
            AtomicReference<Integer> sum= new AtomicReference<>(0);
            community.forEach(id->{
                sum.set(sum.get()+this.getEntity(id).getFriends().size());
            });
            if(sum.get()>=maxSum.get()){
                maxSum.set(sum.get());
                rez.set(community);
            }
        });
        return rez.get();
    }

    public List<PrietenieDTO> getPrieteniUtilizator(Utilizator utilizator){
        return utilizator.getFriends().stream()
                .map(uid ->  {
                    Utilizator u = this.getEntity(uid);
                    Tuple<Long,Long> prietenieId = new Tuple<>(utilizator.getId(), u.getId());
                    return new PrietenieDTO(u, this.repoPrietenii.findOne(prietenieId).getDate());
                })
                .collect(Collectors.toList());
    }

    public List<PrietenieDTO> getPrieteniUtilizatorDupaLuna(Utilizator utilizator, Long luna){
        return utilizator.getFriends().stream()
                .map(uid -> {
                    Utilizator u = this.getEntity(uid);
                    Tuple<Long,Long> pid = new Tuple<>(utilizator.getId(), u.getId());
                    return new PrietenieDTO(u, this.repoPrietenii.findOne(pid).getDate());
                })
                .filter(pdto -> {
                    String l = pdto.getDate().toString().split("-")[1];
                    return l.equals(luna.toString());
                })
                .collect(Collectors.toList());
    }
    public List<Utilizator> getPreviousUesrs(Utilizator utilizator,Long idUserCurent, String pattern){
        if(this.repo.getClass().getName().contains("UtilizatorDbRepository")){
            Method m = null;
            try {
                m = this.repo.getClass().getMethod("getPreviousUsers", Utilizator.class,Long.class, String.class);
                return (List<Utilizator>) m.invoke(this.repo,utilizator,idUserCurent,pattern);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<PrietenieDTO> getNextFirends(Utilizator logedUser, Utilizator curentUser){
        List<PrietenieDTO> l = new ArrayList<>();
        if(curentUser == null){
            AtomicReference<Integer> contor = new AtomicReference<>(0);
            logedUser.getFriends().stream()
                    .forEach(idPrieten-> {
                        if (contor.get() < 6) {
                            Utilizator u = this.repo.findOne(idPrieten);
                            Prietenie p = this.repoPrietenii.findOne(new Tuple<>(logedUser.getId(), idPrieten));
                            l.add(new PrietenieDTO(u, p.getDate()));
                            contor.set(contor.get() + 1);
                        }
                    });
        }
        else {
            Utilizator u = null;
            int j=-1;
            for(int i=0;i<logedUser.getFriends().size(); i++){
                u = this.repo.findOne(logedUser.getFriends().get(i));
                if(u.equals(curentUser)){
                    j=i;
                    break;
                }
            }
            if(u!=null && j != -1){
                for(int i=j+1;i<j+7;i++) {
                    if (i < logedUser.getFriends().size()) {
                        u = this.repo.findOne(logedUser.getFriends().get(i));
                        Prietenie p = this.repoPrietenii.findOne(new Tuple<>(logedUser.getId(), u.getId()));
                        l.add(new PrietenieDTO(u,p.getDate()));
                    }
                }
            }
        }
        return l;
    }

    public List<PrietenieDTO> getPreviousFriends(Utilizator logedUser, Utilizator curentUser){
        List<PrietenieDTO> l = new ArrayList<>();
        int contor = 0;
        Utilizator u = null;
        int j = -1;
        for(int i=logedUser.getFriends().size() - 1; i>-1; i--){
            u = this.repo.findOne(logedUser.getFriends().get(i));
            if(curentUser.equals(u)){
                j=i;
                break;
            }
        }
        if(u!=null && j != -1){
            for(int i=j-6; i<j; i++){
                if(i>=0) {
                    u = this.repo.findOne(logedUser.getFriends().get(i));
                    Prietenie p = this.repoPrietenii.findOne(new Tuple<>(logedUser.getId(), u.getId()));
                    l.add(new PrietenieDTO(u,p.getDate()));
                }
            }
        }
        return l;
    }

    public List<Utilizator> getNextUesrs(Utilizator utilizator, Long idUserCurent, String pattern){
        if(this.repo.getClass().getName().contains("UtilizatorDbRepository")){
            Method m = null;
            try {
                m = this.repo.getClass().getMethod("getNextUsers", Utilizator.class, Long.class, String.class);
                return (List<Utilizator>) m.invoke(this.repo,utilizator,idUserCurent, pattern);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
