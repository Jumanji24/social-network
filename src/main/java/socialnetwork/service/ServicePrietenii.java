package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.service.validators.Validator;

import java.util.ArrayList;
import java.util.List;

public class ServicePrietenii extends  Service<Tuple<Long,Long>, Prietenie> {

    private Repository<Long, Utilizator> repoUtilizatori;
    private Repository<Long, Message> repoMesaje;
    private Repository<Long, CererePrietenie> repoCereriPrietnii;
    private Repository<Long, Event> repoEvents;


    @Override
    public Prietenie deleteEntity(Prietenie entity) {
        Prietenie p = super.deleteEntity(entity);
        Utilizator u1 = this.repoUtilizatori.findOne(p.getId().getLeft());
        Utilizator u2 = this.repoUtilizatori.findOne(p.getId().getRight());
        u1.removePrieten(u2);
        u2.removePrieten(u1);
        return p;
    }

    public ServicePrietenii(Repository<Tuple<Long,Long>, Prietenie> repoPrietenii, Validator<Prietenie> validator, Repository<Long,Utilizator> repoUtilizatori, Repository<Long, Message> repoMesaje, Repository<Long, CererePrietenie> repoCereriPrietenii, Repository<Long, Event> repoEvents) {
        super(repoPrietenii, validator);
        this.repoUtilizatori = repoUtilizatori;
        this.repoMesaje = repoMesaje;
        this.repoCereriPrietnii = repoCereriPrietenii;
        this.repoEvents = repoEvents;
    }

    public ServicePrietenii(Repository<Tuple<Long, Long>, Prietenie> repo, Validator<Prietenie> validator) {
        super(repo, validator);
    }

    public void deletePrietenii(Utilizator user){
        List<Prietenie> l = new ArrayList<>();
        this.getAll().forEach(prietenie -> {
            if(prietenie.getId().getLeft().equals(user.getId()) || prietenie.getId().getRight().equals(user.getId())) {
                l.add(prietenie);
            }
        });
        l.forEach(prietenie -> this.deleteEntity(prietenie));
    }
}
