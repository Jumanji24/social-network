package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.service.validators.Validator;

import java.util.ArrayList;
import java.util.List;

public class ServiceCereriPrietenie extends Service<Long, CererePrietenie> {

    private Repository<Tuple<Long,Long>, Prietenie> repoPrietenii;
    private Repository<Long, Message> repoMesaje;
    private Repository<Long,Utilizator> repoUtilizatori;
    private Repository<Long, Event> repoEvents;



    public ServiceCereriPrietenie(Repository<Long, CererePrietenie> repoCereriPrietenii, Validator<CererePrietenie> validator, Repository<Tuple<Long,Long>,Prietenie> repoPrietenii, Repository<Long, Message> repoMesaje, Repository<Long, Utilizator> repoUtilizatori, Repository<Long, Event> repoEvents) {
        super(repoCereriPrietenii, validator);
        this.repoPrietenii = repoPrietenii;
        this.repoMesaje = repoMesaje;
        this.repoUtilizatori = repoUtilizatori;
        this.repoEvents = repoEvents;
    }

    @Override
    public CererePrietenie addEntity(CererePrietenie entity) {
        Utilizator u = this.repoUtilizatori.findOne(entity.getTo());
        if(u!=null){
            u.addCerereDePrietenie(entity);
        }
        CererePrietenie cererePrietenie = super.addEntity(entity);
        return cererePrietenie;
    }

    @Override
    public CererePrietenie deleteEntity(CererePrietenie entity) {
        CererePrietenie cererePrietenie = super.deleteEntity(entity);
        Utilizator utilizator = this.repoUtilizatori.findOne(entity.getTo());
        utilizator.removeCerere(entity);
        return cererePrietenie;
    }

    public ServiceCereriPrietenie(Repository<Long, CererePrietenie> repo, Validator<CererePrietenie> validator) {
        super(repo, validator);
    }

    public void deleteCereri(Utilizator utilizator){
        List<CererePrietenie> lU = new ArrayList<>();
        this.getAll().forEach(cererePrietenie -> {
            if(cererePrietenie.getFrom().equals(utilizator.getId()) || cererePrietenie.getTo().equals(utilizator.getId())){
                lU.add(cererePrietenie);
            }
        });
        lU.forEach(cererePrietenie -> this.deleteEntity(cererePrietenie));
    }

    public void acceptaCerere(List<Long> idCereri){
        idCereri.forEach(idCerere->{
            CererePrietenie cererePrietenie = this.repo.findOne(idCerere);
            Utilizator u1 = this.repoUtilizatori.findOne(cererePrietenie.getFrom());
            Utilizator u2 = this.repoUtilizatori.findOne(cererePrietenie.getTo());
            this.repoPrietenii.save(new Prietenie(new Tuple<>(u1.getId(),u2.getId())));
            cererePrietenie.setStatus("APPROVED");
            this.repo.update(cererePrietenie);
            u1.addPrieten(u2);
            u2.addPrieten(u1);
        });
    }

    public void respingeCerere(List<Long> idCereri){
        idCereri.forEach(idCerere->{
            CererePrietenie cererePrietenie = this.repo.findOne(idCerere);
            cererePrietenie.setStatus("REJECTED");
            this.repo.update(cererePrietenie);
        });
    }

}
