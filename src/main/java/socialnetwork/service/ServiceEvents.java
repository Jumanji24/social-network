package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.service.validators.Validator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class ServiceEvents extends Service<Long,Event>{
    private Repository<Tuple<Long,Long>, Prietenie> repoPrietenii;
    private Repository<Long, Message> repoMesaje;
    private Repository<Long,Utilizator> repoUtilizatori;
    private Repository<Long, CererePrietenie> repoCereriPrietnii;

    public ServiceEvents(Repository<Long,Event> repoEvents, Validator<Event> validator, Repository<Tuple<Long, Long>, Prietenie> repoPrietenii, Repository<Long, Message> repoMesaje, Repository<Long, Utilizator> repoUtilizatori, Repository<Long, CererePrietenie> repoCereriPrietnii) {
        super(repoEvents, validator);
        this.repoPrietenii = repoPrietenii;
        this.repoMesaje = repoMesaje;
        this.repoUtilizatori = repoUtilizatori;
        this.repoCereriPrietnii = repoCereriPrietnii;
    }
    public List<Event> getNextEvents(Event event){
        if(this.repo.getClass().getName().contains("EventsDbRepository")){
            try {
                Method m = this.repo.getClass().getMethod("getNextEvents",Event.class);
                return (List<Event>) m.invoke(this.repo,event);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public List<Event> getPreviousEvents(Event event){
        if(this.repo.getClass().getName().contains("EventsDbRepository")){
            try {
                Method m = this.repo.getClass().getMethod("getPreviousEvents",Event.class);
                return (List<Event>) m.invoke(this.repo,event);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public Event addToMemory(Event event){
        if(this.repo.getClass().getName().contains("EventsDbRepository")){
            Method m = null;
            try {
                m = this.repo.getClass().getMethod("addToMemory", Event.class);
                return (Event) m.invoke(this.repo,event);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
