package socialnetwork.service.validators;

import socialnetwork.domain.Utilizator;
import socialnetwork.service.validators.errors.ValidationException;

public class ValidatorUtilizator implements Validator<Utilizator> {
    @Override
    public void validate(Utilizator entity) throws ValidationException {

        String msg="";
        if(! entity.getFirstName().matches("^[A-Z]{1}[a-z]+$"))
        {
            msg=msg.concat("Prenumele trebuie sa contina numai litere!");
        }
        if(! entity.getLastName().matches("^[A-Z]{1}[a-z]+$"))
        {
            msg=msg.concat("Numele trebuie sa contina numai litere!");
        }
        if(!msg.equals("")){
            throw new ValidationException(msg);
        }
    }
}
