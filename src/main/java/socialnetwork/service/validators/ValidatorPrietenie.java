package socialnetwork.service.validators;

import socialnetwork.domain.Prietenie;
import socialnetwork.service.validators.errors.ValidationException;

public class ValidatorPrietenie implements Validator<Prietenie> {
    @Override
    public void validate(Prietenie entity) throws ValidationException {
        String msg="";
        if(entity.getId().getLeft().equals(entity.getId().getRight())){
            msg=msg.concat("Id-urile trebuie sa fie diferite!");
        }
        if(!msg.equals("")){
            throw new ValidationException(msg);
        }
    }
}
