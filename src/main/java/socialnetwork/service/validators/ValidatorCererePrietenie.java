package socialnetwork.service.validators;

import socialnetwork.domain.CererePrietenie;
import socialnetwork.service.validators.errors.ValidationException;

public class ValidatorCererePrietenie implements Validator<CererePrietenie> {
    @Override
    public void validate(CererePrietenie entity) throws ValidationException {
        String err="";
        if(entity.getTo().equals(entity.getFrom())){
            err=err.concat("Utilizatorii trebuie sa difere!");
        }
        if(!err.equals("")){
            throw new ValidationException(err);
        }
    }
}
