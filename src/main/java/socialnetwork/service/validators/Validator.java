package socialnetwork.service.validators;

import socialnetwork.service.validators.errors.ValidationException;

public interface Validator<T> {
    void validate(T entity) throws ValidationException;
}