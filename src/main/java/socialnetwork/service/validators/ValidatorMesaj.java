package socialnetwork.service.validators;

import socialnetwork.domain.Message;
import socialnetwork.service.validators.errors.ValidationException;

public class ValidatorMesaj implements Validator<Message> {
    @Override
    public void validate(Message entity) throws ValidationException {
        String err="";
        if(entity.getText().equals("")){
            err=err.concat("Mesajul nu poate fi vid!\n");
        }
        if(entity.getText().length() > 100){
            err=err.concat("Mesajul nu poate avea mai mult de 100 de caractere!\n");
        }
        if(entity.getClass().getName().contains("ReplyMessage")){
            if(entity.getTo().size() != 1){
                err=err.concat("Nu se poate face reply mai multor utilizatori!");
            }
        }
        if (!err.equals("")){
            throw new ValidationException(err);
        }
    }
}
