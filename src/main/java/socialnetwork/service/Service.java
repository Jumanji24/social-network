package socialnetwork.service;

import socialnetwork.domain.Entity;
import socialnetwork.repository.Repository;
import socialnetwork.service.validators.Validator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Observable;

public abstract class Service<ID, E extends Entity<ID>> extends Observable {
    protected Repository<ID,E> repo;
    protected Validator<E> validator;

    public Service(Repository<ID, E> repo, Validator<E> validator) {
        this.repo = repo;
        this.validator = validator;
    }

    public E addEntity(E entity) {
        this.validator.validate(entity);
        return this.repo.save(entity);
    }

    public E deleteEntity(E entity){
        return this.repo.delete(entity.getId());
    }

    public E updateEntity(E entity){
        return this.repo.update(entity);
    }

    public void loadData(Long idUser) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if(this.repo.getClass().getName().contains("UtilizatorDbRepository")){
            Method m = this.repo.getClass().getMethod("loadData",Long.class);
            m.invoke(this.repo,idUser);
        }
        if(this.repo.getClass().getName().contains("PrieteniiDbRepository")){
            Method m = this.repo.getClass().getMethod("loadData",Long.class);
            m.invoke(this.repo,idUser);
        }
        if(this.repo.getClass().getName().contains("MesajeDbRepository")){
            Method m = this.repo.getClass().getMethod("loadData",Long.class);
            m.invoke(this.repo,idUser);
        }
        if(this.repo.getClass().getName().contains("EventsDbRepository")){
            Method m = this.repo.getClass().getMethod("loadData",Long.class);
            m.invoke(this.repo,idUser);
        }
        if(this.repo.getClass().getName().contains("CererePrietenieDbRepository")){
            Method m = this.repo.getClass().getMethod("loadData",Long.class);
            m.invoke(this.repo,idUser);
        }
    }

    public E getEntity(ID id){
        return this.repo.findOne(id);
    }

    public Collection<E> getAll(){
        return repo.findAll();
    }

    public void setChanged(){
        super.setChanged();
    }
}
