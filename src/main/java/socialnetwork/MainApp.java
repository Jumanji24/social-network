package socialnetwork;

import javafx.application.Application;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controller.FactoryStage;
import socialnetwork.domain.*;
import socialnetwork.repository.database.*;
import socialnetwork.service.*;
import socialnetwork.service.validators.*;
import socialnetwork.repository.Repository;


public class MainApp extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        String urlDatabase = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork");
        Repository<Long,Utilizator> repoUtilizatori = new UtilizatorDbRepository(urlDatabase);
        Repository<Tuple<Long,Long>,Prietenie> repoPrietenii = new PrieteniiDbRepository(urlDatabase);
        Repository<Long, Message> repoMesaje = new MesajeDbRepository(urlDatabase);
        Repository<Long,CererePrietenie> repoCereriDePrietenie = new CererePrietenieDbRepository(urlDatabase);
        Repository<Long, Event> repoEvents = new EventsDbRepository(urlDatabase);
        ServiceUtilizatori serviceUtilizatori = new ServiceUtilizatori(repoUtilizatori,new ValidatorUtilizator(),repoPrietenii, repoMesaje, repoCereriDePrietenie, repoEvents);
        ServicePrietenii servicePrietenii = new ServicePrietenii(repoPrietenii, new ValidatorPrietenie(),repoUtilizatori,repoMesaje,repoCereriDePrietenie, repoEvents);
        ServiceMesaje serviceMesaje = new ServiceMesaje(repoMesaje,new ValidatorMesaj(),repoPrietenii,repoUtilizatori,repoCereriDePrietenie, repoEvents);
        ServiceCereriPrietenie serviceCereriPrietenie = new ServiceCereriPrietenie(repoCereriDePrietenie,new ValidatorCererePrietenie(),repoPrietenii,repoMesaje,repoUtilizatori, repoEvents);
        ServiceEvents serviceEvetns = new ServiceEvents(repoEvents,new ValidatorEvent(),repoPrietenii,repoMesaje,repoUtilizatori,repoCereriDePrietenie);

        String view = "/view/viewLogIn.fxml";
        primaryStage = FactoryStage.getStage(view,serviceUtilizatori,servicePrietenii,serviceMesaje,serviceCereriPrietenie,serviceEvetns,null,null,null, null, null, null);
        primaryStage.show();
    }
}


