package socialnetwork.domain;

import java.time.LocalDate;

public class PrietenieDTO {

    Utilizator utilizator;
    LocalDate date;

    public PrietenieDTO(Utilizator utilizator, LocalDate date) {
        this.utilizator = utilizator;
        this.date = date;
    }

    public Utilizator getUtilizator() {
        return utilizator;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return utilizator.getLastName()+" | "+ utilizator.getFirstName() + " | " + this.date;
    }
}