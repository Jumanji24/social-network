package socialnetwork.domain;

import java.util.List;

public class ReplyMessage extends Message {
    private Long idMsgToReply;
    public ReplyMessage(Long id, Long from, String text, List<Long> to, Long idMsgToReply) {
        super(id, from, text, to);
        this.idMsgToReply = idMsgToReply;
    }

    @Override
    public String toString() {
        return "ReplyMesaj{" +
                "msgToReply=" + idMsgToReply + " " +
                 super.toString() + "} ";
    }
}
