package socialnetwork.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Event extends Entity<Long> {
    private String nume;
    private String descriere;
    private LocalDate data;
    private List<Long> users;
    private Utilizator currentUser;

    public Event(Long idEvent, String nume, String descriere, LocalDate data) {
        super.setId(idEvent);
        this.nume = nume;
        this.descriere = descriere;
        this.data = data;
        this.users = new ArrayList<>();
        this.currentUser = null;
    }

    public String getNume() {
        return nume;
    }

    public List<Long> getUsers() {
        return users;
    }

    public Utilizator getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Utilizator currentUser) {
        this.currentUser = currentUser;
    }

    public void addUser(Long idUser){
        this.users.add(idUser);
    }

    public String getDescriere() {
        return descriere;
    }

    public LocalDate getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Event event = (Event) o;
        return event.getId().equals(((Event) o).getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), nume, descriere, data, users);
    }
}
