package socialnetwork.domain;

import java.time.LocalDate;

enum Status{
    PENDING,
    APPROVED,
    REJECTED
}

public class CererePrietenie extends Entity<Long> {
    private Long from;
    private Long to;
    private Status status;
    private LocalDate data;

    public CererePrietenie(Long id, Long from, Long to) {
        super.setId(id);
        this.from = from;
        this.to = to;
        this.status = Status.PENDING;
        this.data = LocalDate.now();
    }

    public CererePrietenie(Long id, Long from, Long to, String status, LocalDate data) {
        super.setId(id);
        this.from = from;
        this.to = to;
        this.status = Status.valueOf(status);
        this.data = data;
    }

    public Long getFrom() {
        return from;
    }

    public Long getTo() {
        return to;
    }

    public LocalDate getData() {
        return data;
    }

    public String getStatus() {
        return status.toString();
    }


    public void setStatus(String status) {
        this.status = Status.valueOf(status);
    }

    @Override
    public String toString() {
        return "\tID: " + this.getId() + " | From: " + this.getFrom() + " | " + this.getStatus();
    }
}
