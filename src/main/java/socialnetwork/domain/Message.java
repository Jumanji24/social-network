package socialnetwork.domain;


import java.time.LocalDate;
import java.util.List;

public class Message extends Entity<Long> {
    Long from;
    String text;
    List<Long> to;
    LocalDate date;

    public Message(Long id, Long from, String text, List<Long> to) {
        super.setId(id);
        this.from = from;
        this.to = to;
        this.text = text;
        this.date = LocalDate.now();
    }

    public Message(Long id, Long from, String text, List<Long> to, LocalDate data) {
        super.setId(id);
        this.from = from;
        this.to = to;
        this.text = text;
        this.date = data;
    }

    public Long getFrom() {
        return from;
    }

    public List<Long> getTo() {
        return to;
    }

    public String getText() {
        return text;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return super.toString() + " Mesaj{" +
                "from=" + from +
                ", text='" + text + '\'' +
                ", to=" + to +
                ", date=" + date +
                '}';
    }
}
