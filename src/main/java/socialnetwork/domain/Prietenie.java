package socialnetwork.domain;

import java.time.LocalDate;


public class Prietenie extends Entity<Tuple<Long,Long>> {

    LocalDate date;

    public Prietenie(Tuple<Long,Long> id) {
        super.setId(id);
        this.date = LocalDate.now();
    }

    public Prietenie(Tuple<Long, Long> id, LocalDate date) {
        super.setId(id);
        this.date = date;
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return super.toString() + "Prietenie{" +
                "date=" + date +
                '}';
    }
}
