package socialnetwork.domain;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Utilizator extends Entity<Long>{
    private String firstName;
    private String lastName;
    private List<Long> friends;
    private boolean visited;
    private List<CererePrietenie> cereri;
    private List<Event> events;
    private List<Event> notifyEvents;

    private int hashValue;
    private String imgPath;
    private ImageView profilePicture;

    public Utilizator(Long id, String firstName, String lastName, int hashValue) {
        super.setId(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.friends = new ArrayList<>();
        this.visited = false;
        this.cereri = new ArrayList<>();
        this.events = new ArrayList<>();
        this.hashValue = hashValue;
        this.imgPath = "/pictures/defaultPicture.jpg";
        this.profilePicture = new ImageView(new Image(this.imgPath));
        this.profilePicture.setFitWidth(40);
        this.profilePicture.setFitHeight(40);
    }

    public Utilizator(Long id, String firstName, String lastName, int hashValue, String imgPath, ImageView profilePicture) {
        super.setId(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.friends = new ArrayList<>();
        this.visited = false;
        this.cereri = new ArrayList<>();

        this.hashValue = hashValue;
        this.imgPath = imgPath;
        this.profilePicture = profilePicture;
        this.events = new ArrayList<>();
        this.notifyEvents = new ArrayList<>();
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getHashValue() {
        return hashValue;
    }

    public List<Event> getEvents() {
        return events;
    }

    public List<Event> getNotifyEvents() {
        return notifyEvents;
    }
    public String getImgPath() {
        return imgPath;
    }

    public ImageView getProfilePicture() {
        return profilePicture;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public void setProfilePicture(ImageView profilePicture) {
        this.profilePicture = profilePicture;
    }

    public void addPrieten(Utilizator user){
        this.friends.add(user.getId());
    }

    public void removePrieten(Utilizator user){
        this.friends.remove(user.getId());
    }

    public List<Long> getFriends() {
        return friends;
    }

    public void setFriends(List<Long> friends) {
        this.friends = friends;
    }

    public List<CererePrietenie> getCereri() {
        return cereri;
    }

    public void removeCerere(CererePrietenie cererePrietenie){
        this.cereri.remove(cererePrietenie);
    }

    public void addCerereDePrietenie(CererePrietenie cererePrietenie){
        this.cereri.add(cererePrietenie);
    }

    public void addEvent(Event event){
        this.events.add(event);
    }
    public void addInterestedEvent(Event event){
        this.notifyEvents.add(event);
    }


    @Override
    public String toString() {
        return super.toString() + " Utilizator{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", friends=" + friends +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilizator)) return false;
        Utilizator that = (Utilizator) o;
        return this.getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getFriends());
    }
}