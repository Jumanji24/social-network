package socialnetwork.repository.file;

import socialnetwork.domain.Utilizator;
import socialnetwork.service.validators.Validator;

import java.util.List;

public class UtilizatorFile extends AbstractFileRepository<Long, Utilizator>{

    public UtilizatorFile(String fileName) {
        super(fileName);
    }

    @Override
    public Utilizator extractEntity(List<String> attributes) {
        //TODO: implement method
        Utilizator user = new Utilizator(Long.parseLong(attributes.get(0)),attributes.get(1),attributes.get(2),0);
        return user;
    }

    @Override
    protected String createEntityAsString(Utilizator entity) {
        return entity.getId()+";"+entity.getFirstName()+";"+entity.getLastName();
    }
}
