package socialnetwork.repository.file;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import java.time.LocalDate;
import java.util.List;

public class PrietenieFile extends AbstractFileRepository<Tuple<Long,Long>, Prietenie> {
    public PrietenieFile(String fileName) {
        super(fileName);
    }

    @Override
    public Prietenie extractEntity(List<String> attributes) {
        Long id1=Long.parseLong(attributes.get(0));
        Long id2=Long.parseLong(attributes.get(1));
        LocalDate date = LocalDate.parse(attributes.get(2));
        Prietenie prietenie = new Prietenie(new Tuple<>(id1,id2), date);
        return prietenie;
    }

    @Override
    protected String createEntityAsString(Prietenie entity) {
        return entity.getId().getLeft() + ";" + entity.getId().getRight() + ";" + entity.getDate();
    }
}
