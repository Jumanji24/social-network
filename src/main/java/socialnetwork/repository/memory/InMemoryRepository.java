package socialnetwork.repository.memory;

import socialnetwork.domain.Entity;
import socialnetwork.service.validators.errors.RepositoryException;
import socialnetwork.repository.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID,E> {

    protected Map<ID,E> entities;

    public InMemoryRepository() {
        entities = new HashMap<ID,E>();
    }

    @Override
    public E findOne(ID id){
        if (id==null)
            throw new RepositoryException("id must be not null");
        return entities.get(id);
    }
    @Override
    public Collection<E> findAll() {
        return entities.values();
    }

    @Override
    public E save(E entity) {
        if (entity==null)
            throw new RepositoryException("entity must be not null");
        if(entities.containsKey(entity.getId())){
            throw new RepositoryException("Exista deja acest Id!");
        }
        return entities.putIfAbsent(entity.getId(),entity);
    }

    @Override
    public E delete(ID id) {
        if(id == null)
            throw new RepositoryException("Id must be not null!");
        if(!entities.containsKey(id)){
            throw new RepositoryException("Nu exista acest Id!");
        }
        return entities.remove(id);
    }

    @Override
    public E update(E entity) {
        if(entity == null)
            throw new RepositoryException("entity must be not null!");
        if(!entities.containsKey(entity.getId())){
            throw new RepositoryException("Nu exista acest Id!");
        }
        Entity e = entities.replace(entity.getId(),entity);
        if(e!=null){
            return null;
        }
        return entity;
    }
}
