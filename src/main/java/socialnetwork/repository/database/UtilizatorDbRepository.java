package socialnetwork.repository.database;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.validators.errors.RepositoryException;

import java.sql.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class UtilizatorDbRepository extends AbstractDatabaseRepository<Long, Utilizator> {

    private HashMap<Integer,Long> usersHashValues;

    public UtilizatorDbRepository(String url) {
        super(url);
    }

    public HashMap<Integer, Long> getUsersHashValues() {
        return usersHashValues;
    }

    private void resetAutoInc(){
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("DBCC CHECKIDENT (Utilizatori, RESEED, 0);");
            statement.execute();
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    public Utilizator addToMemory(Utilizator entity){
        return this.entities.putIfAbsent(entity.getId(), entity);
    }


    @Override
    protected HashSet<Utilizator> extractEntities() {
        if(this.usersHashValues == null){
            this.usersHashValues = new HashMap<>();
        }
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Utilizatori");
            ResultSet resultSet = statement.executeQuery();
            HashSet<Utilizator> rez = new HashSet<>();
            while (resultSet.next()){
                Long id = resultSet.getLong("idUtilizator");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                int hashValue = resultSet.getInt("hashValue");
                String imgPath = resultSet.getString("imgPath");
                ImageView imageView = new ImageView(new Image(imgPath));
                imageView.setFitHeight(40);
                imageView.setFitWidth(40);
                Utilizator utilizator = new Utilizator(id,firstName,lastName,hashValue,imgPath,imageView);
                rez.add(utilizator);
                this.usersHashValues.put(utilizator.getHashValue(),utilizator.getId());
            }
            return rez;
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected HashSet<Utilizator> extractEntities(Long idUser) {
        if(this.usersHashValues == null){
            this.usersHashValues = new HashMap<>();
        }
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Utilizatori WHERE idUtilizator = " + idUser + ";");
            ResultSet resultSet = statement.executeQuery();
            HashSet<Utilizator> rez = new HashSet<>();
            while (resultSet.next()){
                Long id = resultSet.getLong("idUtilizator");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                int hashValue = resultSet.getInt("hashValue");
                String imgPath = resultSet.getString("imgPath");
                ImageView imageView = new ImageView(new Image(imgPath));
                imageView.setFitHeight(40);
                imageView.setFitWidth(40);
                Utilizator utilizator = new Utilizator(id,firstName,lastName,hashValue,imgPath,imageView);
                rez.add(utilizator);
                this.usersHashValues.put(utilizator.getHashValue(),utilizator.getId());
            }
            return rez;
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    public Utilizator getUtilizatorFromDb(Long idUser){
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Utilizatori WHERE idUtilizator = " + idUser + ";");
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                Long id = resultSet.getLong("idUtilizator");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                int hashValue = resultSet.getInt("hashValue");
                String imgPath = resultSet.getString("imgPath");
                ImageView imageView = new ImageView(new Image(imgPath));
                imageView.setFitHeight(40);
                imageView.setFitWidth(40);
                Utilizator utilizator = new Utilizator(id,firstName,lastName,hashValue,imgPath,imageView);
                return utilizator;
            }
            return null;
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    public Utilizator getUtilizatorFromDb(Integer hashing){
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Utilizatori WHERE hashValue = " + hashing + ";");
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                Long id = resultSet.getLong("idUtilizator");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                int hashValue = resultSet.getInt("hashValue");
                String imgPath = resultSet.getString("imgPath");
                ImageView imageView = new ImageView(new Image(imgPath));
                imageView.setFitHeight(40);
                imageView.setFitWidth(40);
                Utilizator utilizator = new Utilizator(id,firstName,lastName,hashValue,imgPath,imageView);
                return utilizator;
            }
            return null;
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected void addToDatabase(Utilizator utilizator) {
        if(this.usersHashValues == null){
            this.usersHashValues = new HashMap<>();
        }
        if(this.usersHashValues.containsKey(utilizator.getHashValue())){
            throw new RepositoryException("There already exists an user with this username!");
        }
        try {
            Connection connection = DriverManager.getConnection(this.url);
            String query = "INSERT INTO Utilizatori (imgPath,hashValue,firstName,lastName) VALUES ('" + utilizator.getImgPath() + "'," + utilizator.getHashValue() + ", '" + utilizator.getFirstName() + "','" + utilizator.getLastName() + "');";
            PreparedStatement statement = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
            statement.execute();
            ResultSet keys = statement.getGeneratedKeys();
            Long key = null;
            if (keys.next()){
                key = keys.getLong(1);
            }
            utilizator.setId(key);
            this.usersHashValues.put(utilizator.getHashValue(), utilizator.getId());
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    public List<Utilizator> getNextUsers(Utilizator utilizator, Long idUserCurent, String pattern){
        try {
            Connection connection = DriverManager.getConnection(this.url);
            if(utilizator == null) {
                List<Utilizator> l = new ArrayList<>();
                PreparedStatement statement = connection.prepareStatement("SELECT TOP 6 * FROM Utilizatori WHERE idUtilizator!=" + idUserCurent + " AND (firstName LIKE '%' + " + "'" + pattern + "'" + " + '%' OR lastName like '%' + " + "'" + pattern + "'" + " + '%') ORDER BY idUtilizator");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    Long id = resultSet.getLong("idUtilizator");
                    String firstName = resultSet.getString("firstName");
                    String lastName = resultSet.getString("lastName");
                    int hashValue = resultSet.getInt("hashValue");
                    String imgPath = resultSet.getString("imgPath");
                    ImageView imageView = new ImageView(new Image(imgPath));
                    imageView.setFitHeight(40);
                    imageView.setFitWidth(40);
                    Utilizator u = new Utilizator(id,firstName,lastName,hashValue,imgPath,imageView);
                    l.add(u);
                }
                return l;
            }
            else {
                List<Utilizator> l = new ArrayList<>();
                PreparedStatement statement = connection.prepareStatement("SELECT TOP 6 * FROM Utilizatori WHERE idUtilizator>" + utilizator.getId() + " AND idUtilizator!=" + idUserCurent + " AND (firstName LIKE '%' + " + "'" + pattern + "'" + " + '%' OR lastName like '%' + " + "'" + pattern + "'" + " + '%') ORDER BY idUtilizator");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    Long id = resultSet.getLong("idUtilizator");
                    String firstName = resultSet.getString("firstName");
                    String lastName = resultSet.getString("lastName");
                    int hashValue = resultSet.getInt("hashValue");
                    String imgPath = resultSet.getString("imgPath");
                    ImageView imageView = new ImageView(new Image(imgPath));
                    imageView.setFitHeight(40);
                    imageView.setFitWidth(40);
                    Utilizator u = new Utilizator(id,firstName,lastName,hashValue,imgPath,imageView);
                    l.add(u);
                }
                return l;
            }
        } catch (SQLException exception) {
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    public List<Utilizator> getPreviousUsers(Utilizator utilizator, Long idUserCurent, String pattern){
        try {
            Connection connection = DriverManager.getConnection(this.url);
            List<Utilizator> l = new ArrayList<>();

            PreparedStatement statement = connection.prepareStatement("SELECT TOP 6 * FROM Utilizatori WHERE idUtilizator<" + utilizator.getId() + " AND idUtilizator!=" + idUserCurent + " AND (firstName LIKE '%' + " + "'" + pattern + "'" + " + '%' OR lastName like '%' + " + "'" + pattern + "'" + " + '%') ORDER BY idUtilizator");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Long id = resultSet.getLong("idUtilizator");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                int hashValue = resultSet.getInt("hashValue");
                String imgPath = resultSet.getString("imgPath");
                ImageView imageView = new ImageView(new Image(imgPath));
                imageView.setFitHeight(40);
                imageView.setFitWidth(40);
                Utilizator u = new Utilizator(id,firstName,lastName,hashValue,imgPath,imageView);
                l.add(u);
            }
            return l;
        } catch (SQLException exception) {
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected void deleteFromDatabase(Utilizator utilizator) {
        try {
            Connection connection = DriverManager.getConnection(this.url);
            String query2 = "DELETE FROM Utilizatori WHERE idUtilizator = " + utilizator.getId() + ";";
            PreparedStatement statement2 = connection.prepareStatement(query2);
            statement2.execute();
            if(this.entities.isEmpty()){
                this.resetAutoInc();
            }
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    private void updateDatabase(Utilizator utilizator) {
        try{
           Connection connection = DriverManager.getConnection(this.url);
           String query1 = "UPDATE Utilizatori SET firstname='"+utilizator.getFirstName() + "', lastName='" + utilizator.getLastName() + "', imgPath = '" + utilizator.getImgPath() + "' WHERE idUtilizator =  " + utilizator.getId() + ";";
           PreparedStatement statement1 = connection.prepareStatement(query1);
           statement1.execute();
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    public Utilizator update(Utilizator utilizator) {
        Utilizator u = super.update(utilizator);
        this.updateDatabase(utilizator);
        return u;
    }
}