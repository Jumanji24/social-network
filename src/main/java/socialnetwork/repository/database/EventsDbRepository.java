package socialnetwork.repository.database;

import socialnetwork.domain.Event;
import socialnetwork.service.validators.errors.RepositoryException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class EventsDbRepository extends AbstractDatabaseRepository<Long, Event> {

    public EventsDbRepository(String url) {
        super(url);
    }

    private void resetAutoInc() {
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("DBCC CHECKIDENT (Events, RESEED, 0);");
            statement.execute();
            PreparedStatement statement2 = connection.prepareStatement("DBCC CHECKIDENT (EventsUtilizatori, RESEED, 0);");
            statement2.execute();
        } catch (SQLException e) {
            throw new RepositoryException("Eroare la baza de date!");
        }
    }


        @Override
    protected HashSet<Event> extractEntities() {
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Events;");
            ResultSet resultSet = statement.executeQuery();
            HashSet<Event> set = new HashSet<>();
            while (resultSet.next()){
                Long idEvent = resultSet.getLong("idEvent");
                String nume = resultSet.getString("nume");
                String descriere = resultSet.getString("descriere");
                LocalDate date = resultSet.getDate("data").toLocalDate();
                Event e = new Event(idEvent,nume,descriere,date);
                set.add(e);
                PreparedStatement statement2 = connection.prepareStatement("SELECT * FROM EventsUtilizatori WHERE idEvent = " + idEvent + ";");
                ResultSet resultSet2 = statement2.executeQuery();
                while (resultSet2.next()) {
                    Long idUser = resultSet2.getLong("idUtilizator");
                    e.addUser(idUser);
                }
            }
            return set;
        } catch (SQLException exception) {
            throw new RepositoryException("Eroare la baza de date");
        }
    }

    @Override
    protected HashSet<Event> extractEntities(Long idUser) {
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Events E INNER JOIN EventsUtilizatori EU ON E.idEvent = EU.idEvent AND EU.idUtilizator = " + idUser + ";");
            ResultSet resultSet = statement.executeQuery();
            HashSet<Event> set = new HashSet<>();
            while (resultSet.next()){
                Long idEvent = resultSet.getLong("idEvent");
                String nume = resultSet.getString("nume");
                String descriere = resultSet.getString("descriere");
                LocalDate date = resultSet.getDate("data").toLocalDate();
                Event e = new Event(idEvent,nume,descriere,date);
                PreparedStatement statement2 = connection.prepareStatement("SELECT Eu.idUtilizator FROM Events E INNER JOIN EventsUtilizatori EU ON E.idEvent = EU.idEvent AND E.idEvent = " + e.getId() + ";");
                ResultSet resultSet2 = statement2.executeQuery();
                while (resultSet2.next()){
                    Long idUsr = resultSet2.getLong(1);
                    e.addUser(idUsr);
                }
                set.add(e);
            }
            return set;
        } catch (SQLException exception) {
            throw new RepositoryException("Eroare la baza de date");
        }
    }

    @Override
    protected void addToDatabase(Event entity) {
        try{
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("INSERT INTO Events(nume,descriere,data) VALUES('" + entity.getNume() +"', '" + entity.getDescriere() + "','" + entity.getData() + "');",
                    Statement.RETURN_GENERATED_KEYS);
            statement.execute();
            ResultSet keys = statement.getGeneratedKeys();
            Long key = null;
            if (keys.next()){
                key = keys.getLong(1);
            }
            entity.setId(key);
        } catch (SQLException exception) {
            throw  new RepositoryException("Eroare la baza de date");
        }
    }

    @Override
    protected void deleteFromDatabase(Event entity) {
        try{
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Events WHERE idEvent = " + entity.getId() + ";");
            statement.execute();
        } catch (SQLException exception) {
            throw new RepositoryException("Eroare la baza de date");
        }
    }

    @Override
    public Event update(Event entity) {
        try{
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT COUNT (*) FROM EventsUtilizatori WHERE idEvent = " + entity.getId() + ";");
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                int numberOfRows = resultSet.getInt(1);
                if (numberOfRows < entity.getUsers().size()) {
                    PreparedStatement statement2 = connection.prepareStatement("INSERT INTO EventsUtilizatori(idUtilizator,idEvent) VALUES(" + entity.getUsers().get(entity.getUsers().size() - 1) + ", " + entity.getId() + ");");
                    statement2.execute();
                } else {
                    PreparedStatement statement2 = connection.prepareStatement("DELETE FROM EventsUtilizatori WHERE idEvent = " + entity.getId() + " AND idUtilizator = " + entity.getCurrentUser().getId() + ";");
                    statement2.execute();
                }
                return super.update(entity);
            }
        } catch (SQLException exception) {
            throw new RepositoryException("Eroare la baza de date");
        }
        return null;
    }

    public List<Event> getNextEvents(Event event){
        try {
            Connection connection = DriverManager.getConnection(this.url);
            if(event == null) {
                PreparedStatement statement = connection.prepareStatement("SELECT TOP 3 * FROM Events ORDER BY idEvent");
                ResultSet resultSet = statement.executeQuery();
                List<Event> l = new ArrayList<>();
                while (resultSet.next()) {
                    Long idEvent = resultSet.getLong("idEvent");
                    String nume = resultSet.getString("nume");
                    String descriere = resultSet.getString("descriere");
                    LocalDate date = resultSet.getDate("data").toLocalDate();
                    Event e = new Event(idEvent, nume, descriere, date);
                    l.add(e);
                    PreparedStatement statement2 = connection.prepareStatement("SELECT * FROM EventsUtilizatori WHERE idEvent = " + idEvent + " ORDER BY idEvent;");
                    ResultSet resultSet2 = statement2.executeQuery();
                    while (resultSet2.next()) {
                        Long idUser = resultSet2.getLong("idUtilizator");
                        e.addUser(idUser);
                    }
                }
                return l;
            }
            else {
                PreparedStatement statement = connection.prepareStatement("SELECT TOP 3 * FROM Events WHERE idEvent>" + event.getId() + " ORDER BY idEvent;");
                ResultSet resultSet = statement.executeQuery();
                List<Event> l = new ArrayList<>();
                while(resultSet.next()){
                    Long idEvent = resultSet.getLong("idEvent");
                    String nume = resultSet.getString("nume");
                    String descriere = resultSet.getString("descriere");
                    LocalDate date = resultSet.getDate("data").toLocalDate();
                    Event e = new Event(idEvent, nume, descriere, date);
                    l.add(e);
                    PreparedStatement statement2 = connection.prepareStatement("SELECT * FROM EventsUtilizatori WHERE idEvent = " + idEvent + " ORDER BY idEvent;");
                    ResultSet resultSet2 = statement2.executeQuery();
                    while (resultSet2.next()) {
                        Long idUser = resultSet2.getLong("idUtilizator");
                        e.addUser(idUser);
                    }
                }
                return l;
            }
        } catch (SQLException exception) {
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    public List<Event> getPreviousEvents(Event event){
        try {
            Connection connection = DriverManager.getConnection(this.url);

            PreparedStatement statement = connection.prepareStatement("SELECT TOP 3 * FROM Events WHERE idEvent<" + event.getId() + " ORDER BY idEvent;");
            ResultSet resultSet = statement.executeQuery();
            List<Event> l = new ArrayList<>();
            while (resultSet.next()){
                Long idEvent = resultSet.getLong("idEvent");
                String nume = resultSet.getString("nume");
                String descriere = resultSet.getString("descriere");
                LocalDate date = resultSet.getDate("data").toLocalDate();
                Event e = new Event(idEvent, nume, descriere, date);
                l.add(e);
                PreparedStatement statement2 = connection.prepareStatement("SELECT * FROM EventsUtilizatori WHERE idEvent = " + idEvent + " ORDER BY idEvent;");
                ResultSet resultSet2 = statement2.executeQuery();
                while (resultSet2.next()) {
                    Long idUser = resultSet2.getLong("idUtilizator");
                    e.addUser(idUser);
                }
            }
            return l;
        } catch (SQLException exception) {
            throw new RepositoryException("Eroare la baza de date!");
        }
    }
    public Event addToMemory(Event event){
        return this.entities.putIfAbsent(event.getId(), event);
    }
}
