package socialnetwork.repository.database;

import socialnetwork.domain.Entity;
import socialnetwork.repository.memory.InMemoryRepository;

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractDatabaseRepository<ID,E extends Entity<ID>> extends InMemoryRepository<ID,E> {
    protected String url;
    protected Set<Long> idUsers;

    public AbstractDatabaseRepository(String url) {
        this.url = url;

        //this.loadData();
    }

    private void loadData(){
        this.extractEntities().forEach(entity->{
            super.save(entity);
        });
    }

    public void loadData(Long idUser){
        this.idUsers = new HashSet<>();
        this.extractEntities(idUser).forEach(entity->{
            if(!this.entities.containsKey(entity.getId())) {
                super.save(entity);
            }
        });
    }

    protected abstract HashSet<E> extractEntities();

    protected abstract HashSet<E> extractEntities(Long idUser);

    protected abstract void addToDatabase(E entity);

    protected abstract void deleteFromDatabase(E entity);

    public Set<Long> getIdUsers() {
        return idUsers;
    }

    @Override
    public E save(E entity){
        //pentru prietenii
        if(entity.getId() != null){
            E e = super.save(entity);
            this.addToDatabase(entity);
            return e;
        }
        else{
            this.addToDatabase(entity);
            return super.save(entity);
        }
    }

    @Override
    public E delete(ID id){
        E e = super.delete(id);
        if(e!=null){
            this.deleteFromDatabase(e);
        }
        return e;
    }

    @Override
    public E update(E entity) {
        return super.update(entity);
    }
}
