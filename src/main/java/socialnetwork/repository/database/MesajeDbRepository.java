package socialnetwork.repository.database;

import socialnetwork.domain.Message;
import socialnetwork.domain.ReplyMessage;
import socialnetwork.service.validators.errors.RepositoryException;

import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class MesajeDbRepository extends AbstractDatabaseRepository<Long, Message> {

    public MesajeDbRepository(String url) {
        super(url);
    }

    private void resetAutoInc(){
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("DBCC CHECKIDENT (Mesaje, RESEED, 0);");
            statement.execute();
            PreparedStatement statement2 = connection.prepareStatement("DBCC CHECKIDENT (MesajeToUtilizatori, RESEED, 0);");
            statement2.execute();
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected HashSet<Message> extractEntities() {
        try{
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Mesaje;");
            ResultSet resultSet = statement.executeQuery();
            HashSet<Message> set = new HashSet<>();
            while (resultSet.next()){
                Long id = resultSet.getLong("idMesaj");
                Long idMesajToReply = resultSet.getLong("idMesajToReply");
                Date data = resultSet.getDate("data");
                Long from = resultSet.getLong("fromUtilizator");
                String text = resultSet.getString("text");
                List<Long> to = new ArrayList<>();
                PreparedStatement statement2 = connection.prepareStatement("SELECT * FROM MesajeToUtilizatori WHERE idMesaj = " + id + ";");
                ResultSet resultSet2 = statement2.executeQuery();
                while (resultSet2.next()){
                    Long idUtilizator = resultSet2.getLong("idUtilizator");
                    to.add(idUtilizator);
                }
                if(idMesajToReply==0){
                    set.add(new Message(id,from,text,to,data.toLocalDate()));
                }
                else{
                    set.add(new ReplyMessage(id,from,text,to,idMesajToReply));
                }
            }
            return set;
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected HashSet<Message> extractEntities(Long idUser) {
        try{
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Mesaje WHERE fromUtilizator = " + idUser + ";");
            ResultSet resultSet = statement.executeQuery();
            HashSet<Message> set = new HashSet<>();
            while (resultSet.next()){
                Long id = resultSet.getLong("idMesaj");
                Long idMesajToReply = resultSet.getLong("idMesajToReply");
                Date data = resultSet.getDate("data");
                Long from = resultSet.getLong("fromUtilizator");
                String text = resultSet.getString("text");
                List<Long> to = new ArrayList<>();
                PreparedStatement statement2 = connection.prepareStatement("SELECT * FROM MesajeToUtilizatori WHERE idMesaj = " + id + ";");
                ResultSet resultSet2 = statement2.executeQuery();
                while (resultSet2.next()){
                    Long idUtilizator = resultSet2.getLong("idUtilizator");
                    to.add(idUtilizator);
                    this.idUsers.add(idUtilizator);
                }
                if(idMesajToReply==0){
                    set.add(new Message(id,from,text,to,data.toLocalDate()));
                }
                else{
                    set.add(new ReplyMessage(id,from,text,to,idMesajToReply));
                }
            }
            PreparedStatement statement3 = connection.prepareStatement("SELECT * FROM MesajeToUtilizatori WHERE idUtilizator = " + idUser + ";");
            ResultSet resultSet3 = statement3.executeQuery();
            while (resultSet3.next()){
                Long idMesaj = resultSet3.getLong("idMesaj");
                PreparedStatement statement4 = connection.prepareStatement("SELECT * FROM Mesaje WHERE idMesaj = " + idMesaj + ";");
                ResultSet resultSet4 = statement4.executeQuery();
                while (resultSet4.next()){
                    Long id = resultSet4.getLong("idMesaj");
                    Long idMesajToReply = resultSet4.getLong("idMesajToReply");
                    Date data = resultSet4.getDate("data");
                    Long from = resultSet4.getLong("fromUtilizator");
                    String text = resultSet4.getString("text");
                    List<Long> to = new ArrayList<>();
                    this.idUsers.add(from);
                    PreparedStatement statement2 = connection.prepareStatement("SELECT * FROM MesajeToUtilizatori WHERE idMesaj = " + id + ";");
                    ResultSet resultSet2 = statement2.executeQuery();
                    while (resultSet2.next()){
                        Long idUtilizator = resultSet2.getLong("idUtilizator");
                        to.add(idUtilizator);
                    }
                    if(idMesajToReply==0){
                        set.add(new Message(id,from,text,to,data.toLocalDate()));
                    }
                    else{
                        set.add(new ReplyMessage(id,from,text,to,idMesajToReply));
                    }
                }
            }
            return set;
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected void addToDatabase(Message entity) {
        try{
            Connection connection = DriverManager.getConnection(this.url);
            if(entity.getClass().getName().contains("ReplyMessage")) {
                Long idMsgToReply = null;
                Field f = ReplyMessage.class.getDeclaredField("idMsgToReply");
                f.setAccessible(true);
                idMsgToReply = (Long) f.get(entity);
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO Mesaje(text,idMesajToReply,fromUtilizator,data) VALUES('"+entity.getText()+"',"+idMsgToReply+","+entity.getFrom() + "," + "'" + entity.getDate()+"'"+");",
                        Statement.RETURN_GENERATED_KEYS
                );
                statement.execute();
                ResultSet keys = statement.getGeneratedKeys();
                Long key = null;
                if (keys.next()){
                    key = keys.getLong(1);
                }
                entity.setId(key);
                entity.getTo().forEach(uId->{
                    try {
                        PreparedStatement statement2 = connection.prepareStatement(
                                "INSERT INTO MesajeToUtilizatori(idMesaj,idUtilizator) VALUES("+entity.getId()+","+uId+");"
                        );
                        statement2.execute();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });
            }
            else{
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO Mesaje(text,fromUtilizator,data) VALUES('"+entity.getText()+"',"+entity.getFrom() + "," + "'" + entity.getDate()+"'"+");",
                        Statement.RETURN_GENERATED_KEYS
                );
                statement.execute();
                ResultSet keys = statement.getGeneratedKeys();
                Long key = null;
                if (keys.next()){
                    key = keys.getLong(1);
                }
                entity.setId(key);
                entity.getTo().forEach(uId->{
                    try {
                        PreparedStatement statement2 = connection.prepareStatement(
                                "INSERT INTO MesajeToUtilizatori(idMesaj,idUtilizator) VALUES("+entity.getId()+","+uId+");"
                        );
                        statement2.execute();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });
            }

        }
        catch (SQLException | NoSuchFieldException | IllegalAccessException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected void deleteFromDatabase(Message entity) {
        try{
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM MesajeToUtilizatori WHERE idMesaj="+entity.getId()+";");
            statement.execute();
            PreparedStatement statement2 = connection.prepareStatement(
                    "DELETE FROM Mesaje WHERE idMesaj="+entity.getId()+";"
            );
            statement2.execute();
            if(this.entities.isEmpty()){
                this.resetAutoInc();
            }
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    public void deleteMesajToUtilizator(Long idMesaj, Long idUtilizator){
        try{
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM MesajeToUtilizatori WHERE idMesaj=" + idMesaj + " AND idUtilizator=" +idUtilizator + ";");
            statement.execute();
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    public Message save(Message entity) {
        if(entity.getClass().getName().contains("ReplyMessage")){
            try {
                Field f = entity.getClass().getDeclaredField("idMsgToReply");
                f.setAccessible(true);
                Long idMsgToReply = (Long) f.get(entity);
                Message msToReply = this.findOne(idMsgToReply);
                if(!msToReply.getTo().contains(entity.getFrom())){
                    throw new RepositoryException("Utilizatorul cu id-ul " + entity.getFrom() + " nu a primit mesaj de la utilizatorul cu id-ul " + msToReply.getFrom() + "!");
                }
                if(!entity.getTo().contains(msToReply.getFrom())){
                    throw new RepositoryException("Utilizatorul cu id-ul " + entity.getTo().get(0) + " nu a trimis mesajul caruia vrei sa-i dai reply utilizatorului cu id-ul " + entity.getFrom() +"!");
                }
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return super.save(entity);
    }
}
