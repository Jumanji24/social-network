package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.service.validators.errors.RepositoryException;

import java.sql.*;
import java.util.HashSet;

public class PrieteniiDbRepository extends AbstractDatabaseRepository<Tuple<Long,Long>, Prietenie> {

    public PrieteniiDbRepository(String url) {
        super(url);
    }

    @Override
    protected HashSet<Prietenie> extractEntities() {
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Prietenii");
            ResultSet resultSet = statement.executeQuery();
            HashSet<Prietenie> rez = new HashSet<>();
            while (resultSet.next()){
                Long uid1 = resultSet.getLong("uid1");
                Long uid2 = resultSet.getLong("uid2");
                Date date = resultSet.getDate("friendshipDate");
                Prietenie prietenie = new Prietenie(new Tuple<>(uid1,uid2), date.toLocalDate());
                rez.add(prietenie);
            }
            return rez;
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected HashSet<Prietenie> extractEntities(Long idUser) {
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Prietenii WHERE uid1 = " + idUser + " OR uid2 = " + idUser + ";");
            ResultSet resultSet = statement.executeQuery();
            HashSet<Prietenie> rez = new HashSet<>();
            while (resultSet.next()){
                Long uid1 = resultSet.getLong("uid1");
                Long uid2 = resultSet.getLong("uid2");
                Date date = resultSet.getDate("friendshipDate");
                Prietenie prietenie = new Prietenie(new Tuple<>(uid1,uid2), date.toLocalDate());
                if(uid1.equals(idUser)){
                    this.idUsers.add(uid2);
                }
                else if(uid2.equals(idUser)) {
                    this.idUsers.add(uid1);
                }
                rez.add(prietenie);
            }
            return rez;
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected void addToDatabase(Prietenie entity) {
        try {
            Connection connection = DriverManager.getConnection(this.url);
            String query = "INSERT INTO Prietenii (uid1,uid2,friendshipDate) VALUES (" + entity.getId().getLeft() + "," + entity.getId().getRight() + "," + "'" + entity.getDate() + "');";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.execute();
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected void deleteFromDatabase(Prietenie entity) {
        try {
            Connection connection = DriverManager.getConnection(this.url);
            String query = "DELETE FROM Prietenii WHERE uid1 = " + entity.getId().getLeft() + " AND uid2 = " + entity.getId().getRight() + ";";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.execute();
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }
}
