package socialnetwork.repository.database;

import socialnetwork.domain.CererePrietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.validators.errors.RepositoryException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CererePrietenieDbRepository extends AbstractDatabaseRepository<Long, CererePrietenie> {

    private void resetAutoInc(){
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("DBCC CHECKIDENT (CereriPrietenii, RESEED, 0);");
            statement.execute();
        }
        catch (SQLException e){
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    public CererePrietenieDbRepository(String url) {
        super(url);
    }

    @Override
    protected HashSet<CererePrietenie> extractEntities() {
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM CereriPrietenii");
            ResultSet resultSet = statement.executeQuery();
            HashSet<CererePrietenie> set = new HashSet<>();
            while (resultSet.next()){
                Long id = resultSet.getLong("idCererePrietenie");
                Long from = resultSet.getLong("fromUtilizator");
                Long to = resultSet.getLong("toUtilizator");
                String status = resultSet.getString("status");
                String data = resultSet.getString("data");
                set.add(new CererePrietenie(id,from,to, status, LocalDate.parse(data)));
            }
            return set;
        } catch (SQLException e) {
            throw  new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected HashSet<CererePrietenie> extractEntities(Long idUser) {
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM CereriPrietenii WHERE toUtilizator = " + idUser + " OR fromUtilizator = " + idUser + ";");
            ResultSet resultSet = statement.executeQuery();
            HashSet<CererePrietenie> set = new HashSet<>();
            while (resultSet.next()){
                Long id = resultSet.getLong("idCererePrietenie");
                Long from = resultSet.getLong("fromUtilizator");
                Long to = resultSet.getLong("toUtilizator");
                String status = resultSet.getString("status");
                String data = resultSet.getString("data");
                set.add(new CererePrietenie(id,from,to, status, LocalDate.parse(data)));
                if(to.equals(idUser)){
                    this.idUsers.add(from);
                }
                else if(from.equals(idUser)){
                    this.idUsers.add(to);
                }
            }
            return set;
        } catch (SQLException e) {
            throw  new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    protected void addToDatabase(CererePrietenie entity) {
        try{
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO CereriPrietenii(toUtilizator,fromUtilizator,status,data) VALUES(" + entity.getTo() + "," + entity.getFrom() + "," + "'" + entity.getStatus()+"', '" +entity.getData() + "');",
                    Statement.RETURN_GENERATED_KEYS
            );
            statement.execute();
            ResultSet keys = statement.getGeneratedKeys();
            Long key = null;
            if (keys.next()){
                key = keys.getLong(1);
            }
            entity.setId(key);
        } catch (SQLException e) {
            throw  new RepositoryException("Eroare la baza de date");
        }
    }

    @Override
    protected void deleteFromDatabase(CererePrietenie entity) {
        try {
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM CereriPrietenii WHERE idCererePrietenie="+ entity.getId() + ";");
            statement.execute();
            if(this.entities.isEmpty()){
                this.resetAutoInc();
            }
        } catch (SQLException e) {
            throw new RepositoryException("Eroare la baza de date!");
        }
    }
    private void updateDatabase(CererePrietenie entity){
        try{
            Connection connection = DriverManager.getConnection(this.url);
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE CereriPrietenii SET status=" + "'" + entity.getStatus() + "'" + " WHERE idCererePrietenie=" + entity.getId() + ";");
            statement.execute();
        } catch (SQLException e) {
            throw new RepositoryException("Eroare la baza de date!");
        }
    }

    @Override
    public CererePrietenie update(CererePrietenie entity) {
        CererePrietenie c = super.update(entity);
        if(c==null){
            this.updateDatabase(entity);
        }
        return c;
    }

    public void deleteCereri(Utilizator utilizator){
        List<CererePrietenie> lU = new ArrayList<>();
        this.findAll().forEach(cererePrietenie -> {
            if(cererePrietenie.getFrom().equals(utilizator.getId()) || cererePrietenie.getTo().equals(utilizator.getId())){
                lU.add(cererePrietenie);
            }
        });
        lU.forEach(cererePrietenie -> this.delete(cererePrietenie.getId()));
    }
}
