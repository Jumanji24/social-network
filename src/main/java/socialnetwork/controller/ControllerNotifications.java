package socialnetwork.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.scene.control.Alert;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import socialnetwork.domain.Event;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.Service;
import socialnetwork.service.ServiceEvents;

import java.time.LocalDate;
import java.time.Period;


public class ControllerNotifications {
    private Utilizator utilizator;
    private ServiceEvents serviceEvents;
    private ObservableList<Event> model = FXCollections.observableArrayList();
    @FXML
    private TableView<Event> tableViewEvents;
    @FXML
    private TableColumn<Event,String> tableColumnName;
    @FXML
    private TableColumn<Event,String> tableColumnDescription;
    @FXML
    private TableColumn<Event,String> tableColumnDaysRemaining;

    private void loadModel(){
        this.model.setAll(this.utilizator.getNotifyEvents());
    }

    private void initTable(){
        this.tableColumnName.setCellValueFactory(new PropertyValueFactory<Event,String>("nume"));
        this.tableColumnDescription.setCellValueFactory(new PropertyValueFactory<Event,String>("descriere"));
        this.tableColumnDaysRemaining.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Event, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Event, String> param) {
                if(param != null){
                    Period period = Period.between(LocalDate.now(), param.getValue().getData());
                    return new SimpleStringProperty(String.valueOf(period.getDays()));
                }
                return new SimpleStringProperty("--empty--");
            }
        });
        this.loadModel();
        this.tableViewEvents.setItems(this.model);
        this.tableViewEvents.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    public void setServices(ServiceEvents serviceEvents, Utilizator utilizator){
        this.serviceEvents = serviceEvents;
        this.utilizator = utilizator;
        this.initTable();
    }

    public void buttonMarkAsReadClicked(MouseEvent mouseEvent) {
        ObservableList<Event> events = this.tableViewEvents.getSelectionModel().getSelectedItems();
        if(events.isEmpty()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "You didn't select any events!");
            alert.showAndWait();
            return;
        }
        events.forEach(event -> {
            this.utilizator.getNotifyEvents().remove(event);
        });
        this.loadModel();
        this.serviceEvents.setChanged();
        this.serviceEvents.notifyObservers();
    }

    public void buttonMarkAllAsReadClicked(MouseEvent mouseEvent) {
        this.utilizator.getNotifyEvents().clear();
        this.loadModel();
        this.serviceEvents.setChanged();
        this.serviceEvents.notifyObservers();
    }
}
