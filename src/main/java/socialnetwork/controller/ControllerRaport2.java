package socialnetwork.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.ServiceMesaje;
import socialnetwork.service.ServiceUtilizatori;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ControllerRaport2 {

    private Utilizator utilizator;
    private  Utilizator prieten;
    private ServiceUtilizatori serviceUtilizatori;
    private ServiceMesaje serviceMesaje;
    private LocalDate dataInceput;
    private LocalDate dataFinal;
    ObservableList<Message> model = FXCollections.observableArrayList();

    @FXML
    TextArea textAreaMesaj;
    @FXML
    TableView<Message> tableViewConversatie;
    @FXML
    TableColumn<Message, String> columnConversatie;
    @FXML
    TableColumn<Message, String> columnPrenume;
    @FXML
    TextField textFieldNumePdf;
    @FXML
    private Label labelSavedPdf;
    @FXML
    private Button buttonSalveazaPdf;
    @FXML
    private ProgressIndicator progressIndicatorSavedPdf;

    private void loadModel(){
        this.model.setAll(this.serviceMesaje.getSortedMesaje(this.utilizator,this.prieten).stream()
                .filter(mesaj -> mesaj.getTo().contains(this.utilizator.getId()))
                .filter(mesaj -> mesaj.getDate().isAfter(this.dataInceput) && mesaj.getDate().isBefore(this.dataFinal))
                .collect(Collectors.toList())
        );
    }

    private void initTableView(){
        this.columnConversatie.setCellValueFactory(new PropertyValueFactory<Message, String>("text"));
        this.columnPrenume.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Message, String>, ObservableValue<String>>(){

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Message, String> param) {
                if(param!=null){
                    return new SimpleStringProperty(serviceUtilizatori.getEntity(param.getValue().getFrom()).getFirstName());
                }
                return new SimpleStringProperty("--empty--");
            }
        });
        this.loadModel();
        this.tableViewConversatie.setItems(this.model);
    }



    public void setServices(ServiceUtilizatori serviceUtilizatori, ServiceMesaje serviceMesaje, Utilizator utilizator, LocalDate dataInceput, LocalDate dataFinal, Utilizator prieten) {
        this.serviceUtilizatori = serviceUtilizatori;
        this.serviceMesaje = serviceMesaje;
        this.utilizator = utilizator;
        this.dataInceput = dataInceput;
        this.dataFinal = dataFinal;
        this.prieten = prieten;
        this.initTableView();
    }

    private void addTableHeaderMesaje(PdfPTable table) {
        Stream.of("Prenume", "Mesaj")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private void addRowsTableMesaje(PdfPTable table) {
        this.tableViewConversatie.getItems().forEach(mesaj -> {
            table.addCell(this.serviceUtilizatori.getEntity(mesaj.getFrom()).getFirstName());
            table.addCell(mesaj.getText());
        });
    }


    public void buttonSalveazaPdfClicked(MouseEvent mouseEvent) throws DocumentException, FileNotFoundException {

        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(this.textFieldNumePdf.getText() + ".pdf"));
        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Chunk chunk = new Chunk("Raport mesajele lui: " + this.utilizator.getFirstName() + " " + this.utilizator.getLastName() + " cu " + this.prieten.getFirstName() + " " + this.prieten.getLastName(), font);

        document.open();

        PdfPTable tableMesaje = new PdfPTable(2);
        addTableHeaderMesaje(tableMesaje);
        addRowsTableMesaje(tableMesaje);

        document.add(new Paragraph(chunk));
        document.add(Chunk.NEWLINE);
        document.add(tableMesaje);

        document.close();
        this.buttonSalveazaPdf.setVisible(false);
        this.progressIndicatorSavedPdf.setVisible(true);
        this.progressIndicatorSavedPdf.setProgress(1);
        this.labelSavedPdf.setVisible(true);
    }

}
