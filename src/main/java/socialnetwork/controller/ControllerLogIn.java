package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class ControllerLogIn {
    private ServiceUtilizatori serviceUtilizatori;
    private ServicePrietenii servicePrietenii;
    private ServiceMesaje serviceMesaje;
    private ServiceCereriPrietenie serviceCereriPrietenie;
    private ServiceEvents serviceEvetns;
    @FXML
    TextField textFieldUsername;
    @FXML
    PasswordField passwordField;


    public void setServices(ServiceUtilizatori serviceUtilizatori, ServicePrietenii servicePrietenii, ServiceMesaje serviceMesaje, ServiceCereriPrietenie serviceCereriPrietenie, ServiceEvents serviceEvetns){
        this.serviceUtilizatori = serviceUtilizatori;
        this.servicePrietenii = servicePrietenii;
        this.serviceMesaje = serviceMesaje;
        this.serviceCereriPrietenie = serviceCereriPrietenie;
        this.serviceEvetns = serviceEvetns;
        this.textFieldUsername.setPromptText("Username");
        this.passwordField.setPromptText("Password");
    }


    public void buttonLogInClicked(MouseEvent mouseEvent) throws IOException, InvocationTargetException, NoSuchMethodException, IllegalAccessException, NoSuchFieldException {
        if(this.textFieldUsername.getText().length() < 6 || this.passwordField.getText().length() < 6){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"The username and password must contain at least 6 characters!");
            alert.showAndWait();
            return;
        }
        Utilizator u = this.serviceUtilizatori.getUtilizator(this.textFieldUsername.getText(), this.passwordField.getText());
        if(u==null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"There is no username with the given credentials!");
            alert.showAndWait();
            return;
        }
        this.serviceUtilizatori.loadData(u.getId());
        this.servicePrietenii.loadData(u.getId());
        this.serviceMesaje.loadData(u.getId());
        this.serviceEvetns.loadData(u.getId());
        this.serviceCereriPrietenie.loadData(u.getId());
        this.serviceUtilizatori.updateUsers();
        this.serviceUtilizatori.updateFriendList();
        this.serviceUtilizatori.updateCereriInit();
        this.serviceUtilizatori.updateEvents();
        Utilizator utilizator = this.serviceUtilizatori.getEntity(u.getId());
        String view = "/view/viewProfile.fxml";
        Stage stage = FactoryStage.getStage(view, serviceUtilizatori, servicePrietenii,
                                            serviceMesaje, serviceCereriPrietenie, serviceEvetns,
                                            utilizator,null, null,
                                     null, null,null);
        stage.show();
    }

    public void buttonSignUpClicked(MouseEvent mouseEvent) throws IOException {
        String view = "/view/viewCreateAccount.fxml";
        Stage stage = FactoryStage.getStage(view,serviceUtilizatori,null,
                                 null,null, null,
                                      null,null, null,
                                    null,null,null);
        stage.show();
    }
}
