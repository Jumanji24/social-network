package socialnetwork.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import socialnetwork.domain.PrietenieDTO;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.ServiceMesaje;
import socialnetwork.service.ServicePrietenii;
import socialnetwork.service.ServiceUtilizatori;

import java.io.IOException;

public class ControllerRapoarte {


    private Utilizator utilizator;
    private ServicePrietenii servicePrietenii;
    private ServiceUtilizatori serviceUtilizatori;
    private ServiceMesaje serviceMesaje;
    private ObservableList<PrietenieDTO> model = FXCollections.observableArrayList();

    @FXML
    TableView<PrietenieDTO> tableViewPrieteni;
    @FXML
    TableColumn columnNume;
    @FXML
    TableColumn columnPrenume;
    @FXML
    TableColumn columnData;
    @FXML
    RadioButton radioButtonRaportMesaje;
    @FXML
    RadioButton radioButtonRaportPrieteniiMesaje;
    @FXML
    Button buttonGenerareRaport;
    @FXML
    DatePicker datePickerDataInceput;
    @FXML
    DatePicker datePickerDataFinal;

    public void setServices(ServiceUtilizatori serviceUtilizatori, ServicePrietenii servicePrietenii, ServiceMesaje serviceMesaje, Utilizator utilizator){
        this.serviceUtilizatori = serviceUtilizatori;
        this.servicePrietenii = servicePrietenii;
        this.serviceMesaje = serviceMesaje;
        this.utilizator = utilizator;
        this.initTableView();
    }

    private void loadModel(){
        this.model.setAll(this.serviceUtilizatori.getPrieteniUtilizator(this.utilizator));
    }

    private void initTableView(){
        this.columnNume.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PrietenieDTO, String>, ObservableValue<String>>(){

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<PrietenieDTO, String> param) {
                if(param!=null){
                    return new SimpleStringProperty(param.getValue().getUtilizator().getLastName());
                }
                return new SimpleStringProperty("--empty--");
            }
        });
        this.columnPrenume.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PrietenieDTO, String>, ObservableValue<String>>(){

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<PrietenieDTO, String> param) {
                if(param!=null){
                    return new SimpleStringProperty(param.getValue().getUtilizator().getFirstName());
                }
                return new SimpleStringProperty("--empty--");
            }
        });
        this.columnData.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PrietenieDTO, String>, ObservableValue<String>>(){

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<PrietenieDTO, String> param) {
                if(param!=null){
                    return new SimpleStringProperty(param.getValue().getDate().toString());
                }
                return new SimpleStringProperty("--empty--");
            }
        });
        this.loadModel();
        this.tableViewPrieteni.setItems(this.model);
    }


    public void buttonGenerareRaportClicked(MouseEvent mouseEvent) throws IOException {
        if(!this.radioButtonRaportPrieteniiMesaje.isSelected() && !this.radioButtonRaportMesaje.isSelected()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"Nu ati selectat niciun tip de raport!");
            alert.showAndWait();
            return;
        }
        if(this.radioButtonRaportPrieteniiMesaje.isSelected() && this.radioButtonRaportMesaje.isSelected()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Trebuie sa selectati doar un tip de raport!");
            alert.showAndWait();
            return;
        }
        if(this.datePickerDataInceput.getValue() == null || this.datePickerDataFinal.getValue() == null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Trebuie sa selectati o perioada calendaristica!");
            alert.showAndWait();
            return;
        }
        if(this.radioButtonRaportPrieteniiMesaje.isSelected()){
            String view = "/view/viewRaport1.fxml";
            Stage stage = FactoryStage.getStage(view,serviceUtilizatori,servicePrietenii,serviceMesaje,null,null,utilizator,null, null, this.datePickerDataInceput.getValue(), this.datePickerDataFinal.getValue(), null);
            stage.show();
        }
        else {
            PrietenieDTO prietenieDTO = this.tableViewPrieteni.getSelectionModel().getSelectedItem();
            if(prietenieDTO == null){
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Nu ati selectat niciun utilizator!");
                alert.showAndWait();
                return;
            }
            String view = "/view/viewRaport2.fxml";
            Stage stage = FactoryStage.getStage(view,serviceUtilizatori,servicePrietenii,serviceMesaje,null,null,utilizator,null, null,  this.datePickerDataInceput.getValue(), this.datePickerDataFinal.getValue(), prietenieDTO.getUtilizator());
            stage.show();
        }
    }
}
