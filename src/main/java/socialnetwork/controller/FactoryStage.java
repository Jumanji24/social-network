package socialnetwork.controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.*;

import java.io.IOException;
import java.time.LocalDate;

public class FactoryStage {
    public static Stage getStage(String view, ServiceUtilizatori serviceUtilizatori, ServicePrietenii servicePrietenii,
                                 ServiceMesaje serviceMesaje, ServiceCereriPrietenie serviceCereriPrietenie, ServiceEvents serviceEvetns,
                                 Utilizator utilizator, Utilizator sender, Stage parent,
                                 LocalDate dataInceput, LocalDate dataFinal, Utilizator prieten) throws IOException {

        if(view.equals("/view/viewLogIn.fxml")){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Application.class.getResource(view));
            AnchorPane layout = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(layout));
            ControllerLogIn controllerLogIn = loader.getController();
            controllerLogIn.setServices(serviceUtilizatori,servicePrietenii,serviceMesaje,serviceCereriPrietenie, serviceEvetns);
            stage.setResizable(false);
            return stage;
        }
        else if(view.equals("/view/viewCreateAccount.fxml")){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Application.class.getResource(view));
            AnchorPane layout = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(layout));
            ControllerCreateAccount controllerCreateAccount = loader.getController();
            controllerCreateAccount.setServices(serviceUtilizatori);
            stage.setResizable(false);
            return stage;
        }
        else if(view.equals("/view/viewProfile.fxml")){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Application.class.getResource(view));
            AnchorPane layout = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(layout));
            ControllerProfile controllerProfile = loader.getController();
            controllerProfile.setServices(serviceUtilizatori,servicePrietenii,serviceMesaje,serviceCereriPrietenie,serviceEvetns,utilizator);
            stage.setOnCloseRequest(event -> {
                controllerProfile.removeObservers();
                controllerProfile.clearAll();
            });
            stage.setResizable(false);
            return stage;
        }
        else if(view.equals("/view/viewEvents.fxml")){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Application.class.getResource(view));
            AnchorPane layout = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(layout));
            ControllerEvents controllerEvents = loader.getController();
            controllerEvents.setServices(serviceUtilizatori,serviceEvetns,utilizator);
            stage.setResizable(false);
            return stage;
        }
        else if(view.equals("/view/viewNotifications.fxml")){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Application.class.getResource(view));
            AnchorPane layout = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(layout));
            ControllerNotifications controllerNotifications = loader.getController();
            controllerNotifications.setServices(serviceEvetns,utilizator);
            stage.setResizable(false);
            return stage;
        }
        else if(view.equals("/view/viewRapoarte.fxml")){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Application.class.getResource(view));
            AnchorPane layout = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(layout));
            ControllerRapoarte controllerRapoarte = loader.getController();
            controllerRapoarte.setServices(serviceUtilizatori,servicePrietenii,serviceMesaje,utilizator);
            stage.setResizable(false);
            return stage;
        }
        else if(view.equals("/view/viewRaport1.fxml")){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Application.class.getResource(view));
            AnchorPane layout = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(layout));
            ControllerRaport1 controllerRaport1 = loader.getController();
            controllerRaport1.setServices(serviceUtilizatori,servicePrietenii,serviceMesaje,utilizator, dataInceput, dataFinal);
            stage.setResizable(false);
            return stage;
        }
        else if(view.equals("/view/viewRaport2.fxml")){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Application.class.getResource(view));
            AnchorPane layout = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(layout));
            ControllerRaport2 controllerRaport2 = loader.getController();
            controllerRaport2.setServices(serviceUtilizatori,serviceMesaje,utilizator, dataInceput, dataFinal, prieten);
            stage.setResizable(false);
            return stage;
        }
        return null;
    }
}
