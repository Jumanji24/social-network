package socialnetwork.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import socialnetwork.domain.Message;
import socialnetwork.domain.PrietenieDTO;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.ServiceMesaje;
import socialnetwork.service.ServicePrietenii;
import socialnetwork.service.ServiceUtilizatori;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ControllerRaport1 {

    @FXML
    private TableView<PrietenieDTO> tableViewPrieteni;
    @FXML
    private TableColumn columnNume;
    @FXML
    private TableColumn columnPrenumePrieteni;
    @FXML
    private TableColumn columnData;
    @FXML
    private TableView<Message> tableViewConversatie;
    @FXML
    private TableColumn<Message, String> columnConversatie;
    @FXML
    private TableColumn<Message, String> columnPrenumeMesaje;
    @FXML
    private TextField textFieldNumePdf;
    @FXML
    private Label labelSavedPdf;
    @FXML
    private Button buttonSalveazaPdf;
    @FXML
    private ProgressIndicator progressIndicatorSavedPdf;

    private Utilizator utilizator;
    private ServicePrietenii servicePrietenii;
    private ServiceUtilizatori serviceUtilizatori;
    private ServiceMesaje serviceMesaje;
    private LocalDate dataInceput;
    private LocalDate dataFinal;
    private ObservableList<PrietenieDTO> modelPrietenii = FXCollections.observableArrayList();
    private ObservableList<Message> modelMesaje = FXCollections.observableArrayList();

    private void loadModel(){
        this.modelPrietenii.setAll(this.servicePrietenii.getAll().stream()
                .filter(prietenie -> prietenie.getId().getLeft().equals(this.utilizator.getId()) ||
                        prietenie.getId().getRight().equals(this.utilizator.getId()))
                .filter(prietenie -> prietenie.getDate().isAfter(this.dataInceput) && prietenie.getDate().isBefore(this.dataFinal))
                .sorted((prietenie1, prietenie2) -> prietenie1.getDate().compareTo(prietenie2.getDate()))
                .map(prietenie -> {
                    if(this.utilizator.getId().equals(prietenie.getId().getLeft())){
                        return new PrietenieDTO(this.serviceUtilizatori.getEntity(prietenie.getId().getRight()),prietenie.getDate());
                    }
                    return new PrietenieDTO(this.serviceUtilizatori.getEntity(prietenie.getId().getLeft()),prietenie.getDate());

                })
                .collect(Collectors.toList())
        );
        this.modelMesaje.setAll(this.serviceMesaje.getAll().stream()
                .filter(mesaj -> mesaj.getTo().contains(this.utilizator.getId()))
                .filter(mesaj -> mesaj.getDate().isAfter(this.dataInceput) && mesaj.getDate().isBefore(this.dataFinal))
                .sorted((mesaj1, mesaj2)->mesaj1.getDate().compareTo(mesaj2.getDate()))
                .collect(Collectors.toList())
        );
    }

    private void initTableView(){
        this.columnNume.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PrietenieDTO, String>, ObservableValue<String>>(){

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<PrietenieDTO, String> param) {
                if(param!=null){
                    return new SimpleStringProperty(param.getValue().getUtilizator().getLastName());
                }
                return new SimpleStringProperty("--empty--");
            }
        });
        this.columnPrenumePrieteni.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PrietenieDTO, String>, ObservableValue<String>>(){

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<PrietenieDTO, String> param) {
                if(param!=null){
                    return new SimpleStringProperty(param.getValue().getUtilizator().getFirstName());
                }
                return new SimpleStringProperty("--empty--");
            }
        });
        this.columnData.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PrietenieDTO, String>, ObservableValue<String>>(){

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<PrietenieDTO, String> param) {
                if(param!=null){
                    return new SimpleStringProperty(param.getValue().getDate().toString());
                }
                return new SimpleStringProperty("--empty--");
            }
        });

        this.columnConversatie.setCellValueFactory(new PropertyValueFactory<Message, String>("text"));
        this.columnPrenumeMesaje.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Message, String>, ObservableValue<String>>(){

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Message, String> param) {
                if(param!=null){
                    return new SimpleStringProperty(serviceUtilizatori.getEntity(param.getValue().getFrom()).getFirstName());
                }
                return new SimpleStringProperty("--empty--");
            }
        });

        this.loadModel();
        this.tableViewPrieteni.setItems(this.modelPrietenii);
        this.tableViewConversatie.setItems(this.modelMesaje);

    }

    public void setServices(ServiceUtilizatori serviceUtilizatori, ServicePrietenii servicePrietenii, ServiceMesaje serviceMesaje, Utilizator utilizator, LocalDate dataInceput, LocalDate dataFinal) {
        this.serviceUtilizatori = serviceUtilizatori;
        this.servicePrietenii = servicePrietenii;
        this.serviceMesaje = serviceMesaje;
        this.utilizator = utilizator;
        this.dataInceput = dataInceput;
        this.dataFinal = dataFinal;
        this.initTableView();
    }

    private void addTableHeader(PdfPTable table, String nume) {
        if(nume.equals("prietenii")) {
            Stream.of("Nume", "Prenume", "Data")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                        header.setBorderWidth(2);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        }
        else if (nume.equals("mesaje")) {
            Stream.of("Prenume", "Mesaj")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                        header.setBorderWidth(2);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        }
    }

    private void addRowsTable(PdfPTable table, String nume) {
        if(nume.equals("prietenii")) {
            this.tableViewPrieteni.getItems().forEach(prietenieDTO -> {
                table.addCell(prietenieDTO.getUtilizator().getLastName());
                table.addCell(prietenieDTO.getUtilizator().getFirstName());
                table.addCell(prietenieDTO.getDate().toString());
            });
        }
        else if (nume.equals("mesaje")) {
            this.tableViewConversatie.getItems().forEach(mesaj -> {
                table.addCell(this.serviceUtilizatori.getEntity(mesaj.getFrom()).getFirstName());
                table.addCell(mesaj.getText());
            });
        }
    }


    public void buttonSalveazaPdfClicked(MouseEvent mouseEvent) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(this.textFieldNumePdf.getText() + ".pdf"));

        document.open();

        Font font1 = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Chunk chunk1 = new Chunk("Raport prietnii lui: " + this.utilizator.getFirstName() + " " + this.utilizator.getLastName(), font1);

        PdfPTable tablePrietenii = new PdfPTable(3);
        this.addTableHeader(tablePrietenii,"prietenii");
        this.addRowsTable(tablePrietenii,"prietenii");

        Font font2 = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Chunk chunk2 = new Chunk("Raport mesajele lui: " + this.utilizator.getFirstName() + " " + this.utilizator.getLastName(), font2);

        PdfPTable tableMesaje = new PdfPTable(2);
        this.addTableHeader(tableMesaje,"mesaje");
        this.addRowsTable(tableMesaje,"mesaje");

        document.add(new Paragraph(chunk1));
        document.add(Chunk.NEWLINE);
        document.add(tablePrietenii);
        document.add(Chunk.NEWLINE);
        document.add(new Paragraph(chunk2));
        document.add(Chunk.NEWLINE);
        document.add(tableMesaje);

        document.close();

        this.buttonSalveazaPdf.setVisible(false);
        this.progressIndicatorSavedPdf.setVisible(true);
        this.progressIndicatorSavedPdf.setProgress(1);
        this.labelSavedPdf.setVisible(true);
    }
}

