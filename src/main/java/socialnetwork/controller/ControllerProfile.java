package socialnetwork.controller;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import socialnetwork.domain.*;
import socialnetwork.service.*;
import socialnetwork.service.validators.errors.RepositoryException;
import socialnetwork.service.validators.errors.ValidationException;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.stream.Collectors;


public class ControllerProfile implements Observer {

    private ServiceUtilizatori serviceUtilizatori;
    private ServicePrietenii servicePrietenii;
    private ServiceMesaje serviceMesaje;
    private ServiceCereriPrietenie serviceCereriPrietenie;
    private ServiceEvents serviceEvetns;
    private Utilizator utilizator;
    private Utilizator other;
    private ObservableList<PrietenieDTO> modelFriends = FXCollections.observableArrayList();
    private ObservableList<Utilizator> modelUsers = FXCollections.observableArrayList();
    private ObservableList<Message> modelMessages = FXCollections.observableArrayList();
    private ObservableList<Utilizator> modelFilterUsers = FXCollections.observableArrayList();

    @FXML
    private AnchorPane anchorPaneFriendLayout;
    @FXML
    private AnchorPane anchorPaneMessageLayout;
    @FXML
    private Label labelFriendshipStatus;
    @FXML
    private FontAwesomeIconView iconViewChat;
    @FXML
    private Label labelChat;
    @FXML
    private FontAwesomeIconView iconViewAcceptFriendRequest;
    @FXML
    private Label labelAcceptFriendRequest;
    @FXML
    private FontAwesomeIconView iconViewDeclineFriendRequest;
    @FXML
    private FontAwesomeIconView iconViewRemoveFriend;
    @FXML
    private FontAwesomeIconView iconViewRemoveFriendRequest;
    @FXML
    private Label labelRemoveFriendRequest;
    @FXML
    private Label labelDeclineFriendRequest;
    @FXML
    private FontAwesomeIconView iconViewAddFriend;
    @FXML
    private Label labelFirstLastName;
    @FXML
    private Circle circleProfilePicture;
    @FXML
    private Circle circleFriendProfilePicture;
    @FXML
    private TextField textFieldSearchUsers;
    @FXML
    private Label labelTableTitle;
    @FXML
    private TableView<PrietenieDTO> tableViewFriends;
    @FXML
    private TableColumn<PrietenieDTO, ImageView> tableColumnProfilePictureFriends;
    @FXML
    private TableColumn<PrietenieDTO, String> tableColumnFirstNameFriends;
    @FXML
    private TableColumn<PrietenieDTO, String> tableColumnLastNameFriends;
    @FXML
    private TableColumn<PrietenieDTO, String> tableColumnDateFriends;
    @FXML
    private TableView<Utilizator> tableViewUsers;
    @FXML
    private TableColumn<Utilizator, ImageView> tableColumnProfilePicture;
    @FXML
    private TableColumn<Utilizator, String> tableColumnFirstName;
    @FXML
    private TableColumn<Utilizator, String> tableColumnLastName;
    @FXML
    private TextField textFieldMessageContent;
    @FXML
    private TableView<Message> tableViewMessages;
    @FXML
    private TableColumn<Message, String> tableColumnMessages;
    @FXML
    private Circle circleFriendProfilePictureMessage;
    @FXML
    private Label labelFriendMessageFirstLastName;
    @FXML
    private Label labelFriendFirstLastName;
    @FXML
    private FontAwesomeIconView iconViewSendMessage;
    @FXML
    private FontAwesomeIconView iconViewCloseConversation;
    @FXML
    private TextField textFieldGroupMessage;
    @FXML
    private AnchorPane anchorPaneGroupMessage;
    @FXML
    private Circle circleNotifications;
    @FXML
    private Label labelNotifications;

    private void loadNextModelUsers(Utilizator utilizator, String pattern){
        List<Utilizator> l = this.serviceUtilizatori.getNextUesrs(utilizator, this.utilizator.getId(),pattern);
        if(!l.isEmpty()) {
            this.modelUsers.setAll(l);
        }
    }

    private void loadPreviousModelUsers(Utilizator utilizator, String pattern){
        List<Utilizator> l = this.serviceUtilizatori.getPreviousUesrs(utilizator,this.utilizator.getId(), pattern);
        if(!l.isEmpty()) {
            this.modelUsers.setAll(l);
        }
    }

    private void loadPreviousFriends(Utilizator curentUser){
        List<PrietenieDTO> l = this.serviceUtilizatori.getPreviousFriends(this.utilizator, curentUser);
        if(!l.isEmpty()) {
            this.modelFriends.setAll(l);
        }

    }

    private void loadNextModelFriends(Utilizator curentUser){
        List<PrietenieDTO> l = this.serviceUtilizatori.getNextFirends(this.utilizator,curentUser);
        if(!l.isEmpty()) {
            this.modelFriends.setAll(l);
        }

    }

    private void initTableUsers(){
        this.tableColumnProfilePicture.setCellValueFactory(new PropertyValueFactory<Utilizator, ImageView>("profilePicture"));
        this.tableColumnLastName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("lastName"));
        this.tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<Utilizator,String>("firstName"));
        this.loadNextModelUsers(null,"");
        this.tableViewUsers.setItems(this.modelUsers);
    }

    private void loadModelMessages(){
        this.modelMessages.setAll(this.serviceMesaje.getSortedMesaje(this.utilizator,this.other).stream()
                .filter(message -> {
                    return (message.getFrom().equals(this.utilizator.getId()) && message.getTo().contains(this.other.getId()) ||
                            message.getFrom().equals(this.other.getId()) && message.getTo().contains(this.utilizator.getId()));
                })
                .collect(Collectors.toList())
                );
    }

    private void initTableMessages(){
        this.tableColumnMessages.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Message, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Message, String> param) {
                if(param!=null){
                    if(param.getValue().getFrom().equals(utilizator.getId())){
                        return new SimpleStringProperty("You: " + param.getValue().getText());
                    }
                    return new SimpleStringProperty(serviceUtilizatori.getEntity(param.getValue().getFrom()).getFirstName() + ": " + param.getValue().getText());
                }
                return new SimpleStringProperty("--empty--");
            }
        });
        this.loadModelMessages();
        this.tableViewMessages.setItems(this.modelMessages);
    }

    private void initTableFriends(){
        this.tableColumnProfilePictureFriends.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PrietenieDTO, ImageView>, ObservableValue<ImageView>>(){
            @Override
            public ObservableValue<ImageView> call(TableColumn.CellDataFeatures<PrietenieDTO, ImageView> param) {
                if(param!=null){
                    ImageView imageView = new ImageView(new Image(param.getValue().getUtilizator().getImgPath()));
                    imageView.setFitWidth(40);
                    imageView.setFitHeight(40);
                    return new SimpleObjectProperty<ImageView>(imageView);
                }
                return new SimpleObjectProperty<ImageView>(null);
            }
        });
        this.tableColumnLastNameFriends.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PrietenieDTO, String>, ObservableValue<String>>(){
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<PrietenieDTO, String> param) {
                if(param!=null){
                    return new SimpleStringProperty(param.getValue().getUtilizator().getLastName());
                }
                return new SimpleStringProperty("--empty--");
            }
        });
        this.tableColumnFirstNameFriends.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PrietenieDTO, String>, ObservableValue<String>>(){

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<PrietenieDTO, String> param) {
                if(param!=null){
                    return new SimpleStringProperty(param.getValue().getUtilizator().getFirstName());
                }
                return new SimpleStringProperty("--empty--");
            }
        });
        this.tableColumnDateFriends.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<PrietenieDTO, String>, ObservableValue<String>>(){

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<PrietenieDTO, String> param) {
                if(param!=null){
                    return new SimpleStringProperty(param.getValue().getDate().toString());
                }
                return new SimpleStringProperty("--empty--");
            }
        });
        this.loadNextModelFriends(null);
        this.tableViewFriends.setItems(this.modelFriends);
        this.tableViewFriends.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    private void initLayout(){
        this.labelFirstLastName.setText(this.utilizator.getFirstName()+ " " + this.utilizator.getLastName());
        this.initTableFriends();
        this.circleProfilePicture.setFill(new ImagePattern(this.utilizator.getProfilePicture().getImage()));
        if(!this.utilizator.getNotifyEvents().isEmpty()){
            this.circleNotifications.setVisible(true);
            this.labelNotifications.setVisible(true);
            this.labelNotifications.setText(String.valueOf(this.utilizator.getNotifyEvents().size()));
        }
    }

    private void loadFriendLayout(){
        this.anchorPaneFriendLayout.setVisible(true);
        this.circleFriendProfilePicture.setFill(new ImagePattern(this.other.getProfilePicture().getImage()));
        this.labelFriendFirstLastName.setText(this.other.getFirstName() + " " + this.other.getLastName());
        this.iconViewAddFriend.setVisible(false);
        this.labelFriendshipStatus.setVisible(false);
        this.iconViewChat.setVisible(false);
        this.labelChat.setVisible(false);
        this.iconViewRemoveFriend.setVisible(false);
        this.iconViewRemoveFriendRequest.setVisible(false);
        this.labelRemoveFriendRequest.setVisible(false);
        this.iconViewAcceptFriendRequest.setVisible(false);
        this.labelAcceptFriendRequest.setVisible(false);
        this.iconViewDeclineFriendRequest.setVisible(false);
        this.labelDeclineFriendRequest.setVisible(false);
        //the users are friends
        if(this.utilizator.getFriends().contains(this.other.getId())){
            this.iconViewRemoveFriend.setVisible(true);
            this.labelFriendshipStatus.setVisible(true);
            this.labelFriendshipStatus.setText("Remove friend");
            this.iconViewChat.setVisible(true);
            this.labelChat.setVisible(true);
            return;
        }
        List<CererePrietenie> l = this.serviceCereriPrietenie.getAll().stream()
                .filter(cererePrietenie -> cererePrietenie.getFrom().equals(this.utilizator.getId()) && cererePrietenie.getTo().equals(this.other.getId()) && cererePrietenie.getStatus().equals("PENDING"))
                .collect(Collectors.toList());
        //utilizator sent friend request to other
        if(!l.isEmpty()){
            this.iconViewRemoveFriendRequest.setVisible(true);
            this.labelRemoveFriendRequest.setVisible(true);
            return;
        }
        //utilizator received friend request from other
        l = this.utilizator.getCereri().stream()
                .filter(cererePrietenie -> cererePrietenie.getFrom().equals(this.other.getId()) && cererePrietenie.getStatus().equals("PENDING"))
                .collect(Collectors.toList());
        if(!l.isEmpty()){
            this.iconViewAcceptFriendRequest.setVisible(true);
            this.labelAcceptFriendRequest.setVisible(true);
            this.iconViewDeclineFriendRequest.setVisible(true);
            this.labelDeclineFriendRequest.setVisible(true);
            return;
        }
        //the users are not friends
        this.iconViewAddFriend.setVisible(true);
        this.labelFriendshipStatus.setVisible(true);
        this.labelFriendshipStatus.setText("Add friend");
    }

    public void setServices(ServiceUtilizatori serviceUtilizatori, ServicePrietenii servicePrietenii, ServiceMesaje serviceMesaje, ServiceCereriPrietenie serviceCereriPrietenie, ServiceEvents serviceEvetns, Utilizator utilizator){
        this.serviceUtilizatori = serviceUtilizatori;
        this.serviceUtilizatori.addObserver(this);
        this.servicePrietenii = servicePrietenii;
        this.servicePrietenii.addObserver(this);
        this.serviceMesaje = serviceMesaje;
        this.serviceMesaje.addObserver(this);
        this.serviceCereriPrietenie = serviceCereriPrietenie;
        this.serviceCereriPrietenie.addObserver(this);
        this.serviceEvetns = serviceEvetns;
        this.serviceEvetns.addObserver(this);
        this.utilizator = utilizator;
        this.initLayout();
        this.tableViewFriends.requestFocus();
    }

    public void iconViewChatClicked(MouseEvent mouseEvent) {
        if(this.anchorPaneGroupMessage.isVisible()){
            this.anchorPaneGroupMessage.setVisible(false);
        }
        this.anchorPaneFriendLayout.setVisible(false);
        this.anchorPaneMessageLayout.setVisible(true);
        this.labelFriendMessageFirstLastName.setText(other.getFirstName() + " " + other.getLastName());
        this.circleFriendProfilePictureMessage.setFill(new ImagePattern(other.getProfilePicture().getImage()));
        this.tableViewUsers.requestFocus();
        this.initTableMessages();
    }

    public void iconViewCloseConversationClicked(MouseEvent mouseEvent) {
        this.anchorPaneMessageLayout.setVisible(false);
        this.anchorPaneFriendLayout.setVisible(true);
    }

    public void textFieldSearchUsersTyped(KeyEvent keyEvent) {
        if(this.textFieldSearchUsers.getLength() == 0){
            this.tableViewFriends.setVisible(true);
            this.tableViewUsers.setVisible(false);
            this.labelTableTitle.setText("Friends");
            return;
        }
    }

    public void tableViewFriendsClicked(MouseEvent mouseEvent) {
        PrietenieDTO prietenieDTO = this.tableViewFriends.getSelectionModel().getSelectedItem();
        if(prietenieDTO!=null) {

            this.other = prietenieDTO.getUtilizator();
            this.loadFriendLayout();
        }
    }

    public void tableViewUsersClicked(MouseEvent mouseEvent) {

        this.other = this.tableViewUsers.getSelectionModel().getSelectedItem();
        if(this.other!=null) {
            this.loadFriendLayout();
        }
    }

    public void textFieldSearchUsersClicked(MouseEvent mouseEvent) {
        this.initTableUsers();
        this.tableViewFriends.setVisible(false);
        this.tableViewUsers.setVisible(true);
        this.labelTableTitle.setText("Users");
    }

    public void iconViewChangeProfilePictureClicked(MouseEvent mouseEvent) throws MalformedURLException {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        fileChooser.setTitle("Change profile picture");
        File file = fileChooser.showOpenDialog(stage);
        URL imgPath = null;
        Image image = null;
        if(file != null){
            imgPath = file.toURI().toURL();
            image = new Image(imgPath.toString());
            this.utilizator.setImgPath(imgPath.toString());
            ImageView imageView = new ImageView(image);
            imageView.setFitHeight(40);
            imageView.setFitWidth(40);
            this.utilizator.setProfilePicture(imageView);
            this.serviceUtilizatori.updateEntity(this.utilizator);
            this.circleProfilePicture.setFill(new ImagePattern(this.utilizator.getProfilePicture().getImage()));
            this.serviceUtilizatori.setChanged();
            this.serviceUtilizatori.notifyObservers();
        }
    }

    public void iconViewAddFriendClicked(MouseEvent mouseEvent) {
        Long idTo = this.other.getId();
        Long idFrom = this.utilizator.getId();
        CererePrietenie cererePrietenie = new CererePrietenie(null,idFrom,idTo);
        this.serviceCereriPrietenie.addEntity(cererePrietenie);
        this.serviceCereriPrietenie.setChanged();
        this.serviceCereriPrietenie.notifyObservers();
    }

    public void iconViewAcceptFriendRequestClicked(MouseEvent mouseEvent) {
        this.serviceCereriPrietenie.acceptaCerere(this.serviceCereriPrietenie.getAll().stream()
                .filter(cererePrietenie -> cererePrietenie.getFrom().equals(this.other.getId()) && cererePrietenie.getTo().equals(this.utilizator.getId()) && cererePrietenie.getStatus().equals("PENDING"))
                .map(cererePrietenie -> cererePrietenie.getId())
                .collect(Collectors.toList())
        );
        this.serviceCereriPrietenie.setChanged();
        this.serviceCereriPrietenie.notifyObservers();
    }

    public void iconViewDeclineFriendRequestClicked(MouseEvent mouseEvent) {
        this.serviceCereriPrietenie.respingeCerere(this.serviceCereriPrietenie.getAll().stream()
                .filter(cererePrietenie -> cererePrietenie.getFrom().equals(this.other.getId()) && cererePrietenie.getTo().equals(this.utilizator.getId()) && cererePrietenie.getStatus().equals("PENDING"))
                .map(cererePrietenie -> cererePrietenie.getId())
                .collect(Collectors.toList())
        );
        this.serviceCereriPrietenie.setChanged();
        this.serviceCereriPrietenie.notifyObservers();
    }

    public void iconViewRemoveFriendClicked(MouseEvent mouseEvent) {
        this.servicePrietenii.deleteEntity(new Prietenie(new Tuple<>(this.utilizator.getId(),this.other.getId())));
        this.servicePrietenii.setChanged();
        this.servicePrietenii.notifyObservers();
    }

    public void iconViewRemoveFriendRequestClicked(MouseEvent mouseEvent) {
        List<CererePrietenie> l = this.serviceCereriPrietenie.getAll().stream()
                .filter(cererePrietenie -> cererePrietenie.getFrom().equals(this.utilizator.getId()) && cererePrietenie.getTo().equals(this.other.getId()) && cererePrietenie.getStatus().equals("PENDING"))
                .collect(Collectors.toList());
        this.serviceCereriPrietenie.deleteEntity(l.get(0));
        this.serviceCereriPrietenie.setChanged();
        this.serviceCereriPrietenie.notifyObservers();
    }

    @Override
    public void update(Observable o, Object arg) {
        this.loadNextModelUsers(null,"");
        this.loadNextModelFriends(null);

        if(!this.utilizator.getNotifyEvents().isEmpty()){
            this.circleNotifications.setVisible(true);
            this.labelNotifications.setVisible(true);
            this.labelNotifications.setText(String.valueOf(this.utilizator.getNotifyEvents().size()));
        }
        else{
            this.circleNotifications.setVisible(false);
            this.labelNotifications.setVisible(false);
        }
        if(this.other!=null) {
            this.loadModelMessages();
            this.loadFriendLayout();
        }
    }

    public void removeObservers(){
        this.serviceUtilizatori.deleteObserver(this);
        this.serviceCereriPrietenie.deleteObserver(this);
        this.servicePrietenii.deleteObserver(this);
        this.serviceMesaje.deleteObserver(this);
        this.serviceEvetns.deleteObserver(this);
    }

    public void buttonCloseUserProfileLayoutClicked(MouseEvent mouseEvent) {
        this.other = null;
        this.anchorPaneFriendLayout.setVisible(false);
    }

    public void iconViewSendMessageClicked(MouseEvent mouseEvent) {
        try {
            String messageContent = this.textFieldMessageContent.getText();
            List<Long> l = new ArrayList<>();
            l.add(this.other.getId());
            Message message = new Message(null, this.utilizator.getId(), messageContent, l);
            this.serviceMesaje.addEntity(message);
            this.serviceMesaje.setChanged();
            this.serviceMesaje.notifyObservers();
            this.textFieldMessageContent.clear();
            this.anchorPaneFriendLayout.setVisible(false);
        }
        catch (ValidationException | RepositoryException exception){
            Alert alert = new Alert(Alert.AlertType.INFORMATION, exception.getMessage());
            alert.showAndWait();
        }
    }
    public void iconViewGroupMessageClicked(MouseEvent mouseEvent) {
        if(anchorPaneGroupMessage.isVisible()) {
            this.anchorPaneGroupMessage.setVisible(false);
        }
        else{
            if(this.anchorPaneMessageLayout.isVisible()){
                this.anchorPaneMessageLayout.setVisible(false);
                this.anchorPaneFriendLayout.setVisible(true);
            }
            this.anchorPaneGroupMessage.setVisible(true);
        }
    }

    public void iconViewSendGroupMessageClicked(MouseEvent mouseEvent) {
        ObservableList<PrietenieDTO> l = this.tableViewFriends.getSelectionModel().getSelectedItems();
        if(l.isEmpty()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"You haven't selected the users!");
            alert.showAndWait();
            return;
        }
        l.forEach(prietenieDTO -> {
            String messageContent = this.textFieldGroupMessage.getText();
            List<Long> l2 = new ArrayList<>();
            l2.add(prietenieDTO.getUtilizator().getId());
            Message message = new Message(null,this.utilizator.getId(),messageContent,l2);
            this.serviceMesaje.addEntity(message);
            this.serviceMesaje.setChanged();
        });
        this.textFieldGroupMessage.clear();
        this.anchorPaneGroupMessage.setVisible(false);
    }

    public void iconViewRaportsClicked(MouseEvent mouseEvent) throws IOException {
        String view = "/view/viewRapoarte.fxml";
        Stage stage = FactoryStage.getStage(view,serviceUtilizatori,servicePrietenii,
                                            serviceMesaje,serviceCereriPrietenie, null,
                                            utilizator,null, null,
                                  null, null,null);
        stage.show();
    }

    public void iconViewEventsClicked(MouseEvent mouseEvent) throws IOException {
        String view = "/view/viewEvents.fxml";
        Stage stage = FactoryStage.getStage(view,serviceUtilizatori,null,
                                null,null,serviceEvetns,
                                           utilizator,null,null,
                                 null, null,null);
        stage.show();
    }

    public void iconViewNotifications(MouseEvent mouseEvent) throws IOException {
        String view = "/view/viewNotifications.fxml";
        Stage stage = FactoryStage.getStage(view,null,null,

                                            null,null,serviceEvetns,
                                            utilizator,null,null,
                                            null,null,null);
        stage.show();
    }

    public void buttonPrevPageClicked(MouseEvent mouseEvent) {
        if(this.tableViewUsers.isVisible()) {
            Utilizator utilizator = this.tableViewUsers.getItems().get(0);
            this.loadPreviousModelUsers(utilizator,this.textFieldSearchUsers.getText());
        }
        else if(this.tableViewFriends.isVisible()){
            Utilizator utilizator = this.tableViewFriends.getItems().get(0).getUtilizator();
            this.loadPreviousFriends(utilizator);
        }
    }

    public void buttonNextPageClicked(MouseEvent mouseEvent) {
        if(this.tableViewUsers.isVisible()) {
            Utilizator utilizator = this.tableViewUsers.getItems().get(this.tableViewUsers.getItems().size() - 1);
            this.loadNextModelUsers(utilizator,this.textFieldSearchUsers.getText());
        }
        else if(this.tableViewFriends.isVisible()){
            Utilizator utilizator = this.tableViewFriends.getItems().get(this.tableViewFriends.getItems().size() - 1).getUtilizator();
            this.loadNextModelFriends(utilizator);
        }
    }

    public void iconViewSearchUsersClicked(MouseEvent mouseEvent) {
        this.tableViewFriends.setVisible(false);
        this.tableViewUsers.setVisible(true);
        this.labelTableTitle.setText("Users");
        this.loadNextModelUsers(null,this.textFieldSearchUsers.getText());
    }

    public void iconViewSearchUsersTyped(KeyEvent keyEvent) {
        if(this.textFieldSearchUsers.getLength() == 0){
            this.loadNextModelUsers(null,"");
            return;
        }
    }

    public void clearAll(){
        this.serviceUtilizatori.getAll().clear();
        this.servicePrietenii.getAll().clear();
        this.serviceMesaje.getAll().clear();
        this.serviceCereriPrietenie.getAll().clear();
        this.serviceEvetns.getAll().clear();
    }
}
