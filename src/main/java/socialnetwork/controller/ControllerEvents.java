package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import socialnetwork.domain.Event;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.ServiceEvents;
import socialnetwork.service.ServiceUtilizatori;
import socialnetwork.service.validators.errors.RepositoryException;
import socialnetwork.service.validators.errors.ValidationException;

import java.time.LocalDate;
import java.util.List;

public class ControllerEvents {
    private ServiceUtilizatori serviceUtilizatori;
    private ServiceEvents serviceEvetns;
    private Utilizator utilizator;
    private ObservableList<Event> userEventsModel = FXCollections.observableArrayList();
    private ObservableList<Event> eventsModel = FXCollections.observableArrayList();
    @FXML
    private TableView<Event> tableViewUserEvents;
    @FXML
    private TableColumn<Event, String> tableColumnNameUserEvents;
    @FXML
    private TableColumn<Event, String> tableColumnDescriptionUserEvents;
    @FXML
    private TableColumn<Event, String> tableColumnDateUserEvents;
    @FXML
    private TableView<Event> tableViewEvents;
    @FXML
    private TableColumn<Event, String> tableColumnNameEvents;
    @FXML
    private TableColumn<Event, String> tableColumnDescriptionEvents;
    @FXML
    private TableColumn<Event, String> tableColumnDateEvents;
    @FXML
    private TextField textFieldEventName;
    @FXML
    private TextField textFieldEventDescription;
    @FXML
    private DatePicker datePickerEventDate;
    @FXML
    private Button buttonInterested;
    @FXML
    private Button buttonUniterested;

    private void loadUserEventsModel(){
        this.userEventsModel.setAll(this.utilizator.getEvents());
    }

    private void loadNextEventsModel(Event event){
        List<Event> l = this.serviceEvetns.getNextEvents(event);
        if(!l.isEmpty()){
            this.eventsModel.setAll(l);
        }
    }

    private void loadPreviousEventsModel(Event event){
        List<Event> l = this.serviceEvetns.getPreviousEvents(event);
        if(!l.isEmpty()){
            this.eventsModel.setAll(l);
        }
    }

    private void initUserEventsTableView(){
        this.tableColumnNameUserEvents.setCellValueFactory(new PropertyValueFactory<Event, String>("nume"));
        this.tableColumnDescriptionUserEvents.setCellValueFactory(new PropertyValueFactory<Event, String>("descriere"));
        this.tableColumnDateUserEvents.setCellValueFactory(new PropertyValueFactory<Event, String>("data"));
        this.loadUserEventsModel();
        this.tableViewUserEvents.setItems(this.userEventsModel);
    }

    private void initEventsModelTableView(){
        this.tableColumnNameEvents.setCellValueFactory(new PropertyValueFactory<Event, String>("nume"));
        this.tableColumnDescriptionEvents.setCellValueFactory(new PropertyValueFactory<Event, String>("descriere"));
        this.tableColumnDateEvents.setCellValueFactory(new PropertyValueFactory<Event, String>("data"));
        this.loadNextEventsModel(null);
        this.tableViewEvents.setItems(this.eventsModel);
    }

    public void setServices(ServiceUtilizatori serviceUtilizatori, ServiceEvents serviceEvetns, Utilizator utilizator){
        this.serviceUtilizatori = serviceUtilizatori;
        this.serviceEvetns = serviceEvetns;
        this.utilizator = utilizator;
        this.initUserEventsTableView();
        this.initEventsModelTableView();
    }

    public void buttonUniterestedClicked(MouseEvent mouseEvent) {
        Event event = this.tableViewUserEvents.getSelectionModel().getSelectedItem();
        if(event == null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"You must select one event from your events!");
            alert.showAndWait();
            return;
        }
        this.utilizator.getEvents().remove(event);
        this.utilizator.getNotifyEvents().remove(event);
        event.getUsers().remove(this.utilizator.getId());
        event.setCurrentUser(this.utilizator);
        this.serviceEvetns.updateEntity(event);
        this.loadUserEventsModel();
        this.loadNextEventsModel(null);
        this.serviceEvetns.setChanged();
        this.serviceEvetns.notifyObservers();
    }

    public void buttonInterestedClicked(MouseEvent mouseEvent) {
        Event event = this.tableViewEvents.getSelectionModel().getSelectedItem();
        if(event == null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"You must select one event from existing events!");
            alert.showAndWait();
            return;
        }
        if(this.utilizator.getEvents().contains(event)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "You are already interested in this event!");
            alert.showAndWait();
            return;
        }
        this.utilizator.addEvent(event);
        this.utilizator.addInterestedEvent(event);
        event.addUser(this.utilizator.getId());
        if(this.serviceEvetns.getEntity(event.getId()) == null) {
            this.serviceEvetns.addToMemory(event);
        }
        this.serviceEvetns.updateEntity(event);
        this.loadUserEventsModel();
        this.loadNextEventsModel(null);
        this.serviceEvetns.setChanged();
        this.serviceEvetns.notifyObservers();
    }

    public void buttonAddEventClicked(MouseEvent mouseEvent) {
        if(this.textFieldEventName.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"You must provide a name for the event!");
            alert.showAndWait();
            return;
        }
        if(this.textFieldEventDescription.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"You must provide a description for the event!");
            alert.showAndWait();
            return;
        }
        if(this.datePickerEventDate.getValue().toString().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"You must provide a starting date for the event!");
            alert.showAndWait();
            return;
        }
        if(this.datePickerEventDate.getValue().isBefore(LocalDate.now())){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"You cannot provide a date that is before today!");
            alert.showAndWait();
            return;
        }
        try{
            Event event = new Event(null,this.textFieldEventName.getText(),this.textFieldEventDescription.getText(),this.datePickerEventDate.getValue());
            this.serviceEvetns.addEntity(event);
            this.loadNextEventsModel(null);
        }
        catch (ValidationException | RepositoryException exception){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,exception.getMessage());
            alert.showAndWait();
        }
    }

    public void tableViewUserEventsClicked(MouseEvent mouseEvent) {
        Event event = this.tableViewUserEvents.getSelectionModel().getSelectedItem();
        if (event!=null){
            this.buttonInterested.setVisible(false);
            this.buttonUniterested.setVisible(true);
        }
    }

    public void tableViewEventsClicked(MouseEvent mouseEvent) {
        Event event = this.tableViewEvents.getSelectionModel().getSelectedItem();
        if (event!=null){
            this.buttonUniterested.setVisible(false);
            this.buttonInterested.setVisible(true);
        }
    }

    public void buttonPreviosExistingEventsClicked(MouseEvent mouseEvent) {
        Event event = this.tableViewEvents.getItems().get(0);
        this.loadPreviousEventsModel(event);
    }

    public void buttonNextExistingEventsClicked(MouseEvent mouseEvent) {
        Event event = this.tableViewEvents.getItems().get(this.tableViewEvents.getItems().size() - 1);
        this.loadNextEventsModel(event);
    }
}
