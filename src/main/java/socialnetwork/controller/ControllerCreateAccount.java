package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.ServiceUtilizatori;
import socialnetwork.service.validators.errors.RepositoryException;
import socialnetwork.service.validators.errors.ValidationException;


import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

public class ControllerCreateAccount {

    private ServiceUtilizatori serviceUtilizatori;
    @FXML
    private TextField textFieldFirstName;
    @FXML
    private TextField textFieldLastName;
    @FXML
    private TextField textFieldUsername;
    @FXML
    private PasswordField passwordField;
    @FXML
    private PasswordField passwordFieldVerif;
    @FXML
    private Button buttonCreateAccount;
    @FXML
    private CheckBox checkBoxPasswordVerif;
    @FXML
    private CheckBox checkBoxPassword;
    @FXML
    private TextField textFieldPassword;
    @FXML
    private TextField textFieldPasswordVerif;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private Label labelMessage;


    public void setServices(ServiceUtilizatori serviceUtilizatori){
        this.serviceUtilizatori=serviceUtilizatori;
        this.buttonCreateAccount.requestFocus();
    }

    public void checkBoxShowPasswordVerifClicked(MouseEvent mouseEvent) {
        if(this.checkBoxPasswordVerif.isSelected()){
            this.passwordFieldVerif.setVisible(false);
            this.textFieldPasswordVerif.setVisible(true);
            this.textFieldPasswordVerif.setText(this.passwordFieldVerif.getText());
            this.textFieldPasswordVerif.positionCaret(this.textFieldPasswordVerif.getLength());
        }
        else{
            this.textFieldPasswordVerif.setVisible(false);
            this.passwordFieldVerif.setVisible(true);
            this.passwordFieldVerif.setText(this.textFieldPasswordVerif.getText());
            this.passwordFieldVerif.positionCaret(this.passwordFieldVerif.getLength());
        }
    }

    public void checkBoxShowPasswordClicked(MouseEvent mouseEvent) {
        if(this.checkBoxPassword.isSelected()){
            this.passwordField.setVisible(false);
            this.textFieldPassword.setVisible(true);
            this.textFieldPassword.setText(this.passwordField.getText());
            this.textFieldPassword.positionCaret(this.textFieldPassword.getLength());
        }
        else{
            this.textFieldPassword.setVisible(false);
            this.passwordField.setVisible(true);
            this.passwordField.setText(this.textFieldPassword.getText());
            this.passwordField.positionCaret(this.passwordField.getLength());
        }
    }

    public void buttonCreateAccountClicked(MouseEvent mouseEvent) {
        if(this.checkBoxPassword.isSelected()) {
            this.passwordField.setVisible(true);
            this.passwordField.setText(this.textFieldPassword.getText());
            this.textFieldPassword.setVisible(false);
        }
        if(this.checkBoxPasswordVerif.isSelected()) {
            this.passwordFieldVerif.setVisible(true);
            this.passwordFieldVerif.setText(this.textFieldPasswordVerif.getText());
            this.textFieldPasswordVerif.setVisible(false);
        }
        if(this.textFieldUsername.getText().length() < 6 || this.passwordField.getText().length() < 6){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"The username and password must contain at least 6 characters!");
            alert.showAndWait();
            return;
        }
        if(!this.passwordField.getText().equals(this.passwordFieldVerif.getText())){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"The passwords doesn't match!");
            alert.showAndWait();
            return;
        }
        try {
            if(this.serviceUtilizatori.getUtilizator(this.textFieldUsername.getText(), this.passwordField.getText()) != null){
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "The already exists one user with the this username!");
                alert.showAndWait();
                return;
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        try {
            Utilizator utilizator = new Utilizator(null, this.textFieldFirstName.getText(), this.textFieldLastName.getText(), Objects.hash(this.textFieldUsername.getText(), this.passwordField.getText()));
            this.serviceUtilizatori.addEntity(utilizator);
            this.buttonCreateAccount.setVisible(false);
            this.labelMessage.setVisible(true);
            this.progressIndicator.setVisible(true);
            this.progressIndicator.setProgress(1);
        }
        catch (ValidationException | RepositoryException exception){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,exception.getMessage());
            alert.showAndWait();
        }
    }
}
