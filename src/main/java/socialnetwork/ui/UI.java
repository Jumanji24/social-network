package socialnetwork.ui;

import socialnetwork.domain.*;
import socialnetwork.service.ServiceCereriPrietenie;
import socialnetwork.service.ServiceMesaje;
import socialnetwork.service.ServicePrietenii;
import socialnetwork.service.ServiceUtilizatori;
import socialnetwork.service.validators.errors.RepositoryException;
import socialnetwork.service.validators.errors.ValidationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class UI {
    private ServiceUtilizatori srvUtilizatori;
    private ServicePrietenii srvPrietenii;
    private ServiceMesaje srvMesaje;
    private ServiceCereriPrietenie srvCereriPrietenie;

    public UI(ServiceUtilizatori srvUtilizatori, ServicePrietenii srvPrietenii, ServiceMesaje srvMesaje, ServiceCereriPrietenie srvCereriPrietenie) {
        this.srvUtilizatori = srvUtilizatori;
        this.srvPrietenii = srvPrietenii;
        this.srvMesaje = srvMesaje;
        this.srvCereriPrietenie = srvCereriPrietenie;
    }

    private void printMenu(){
        System.out.println("1. Actiuni utilizatori");
        System.out.println("2. Actiuni prietenii");
        System.out.println("3. Actiuni mesaje");
        System.out.println("4. Actiuni cereri de prietenie");
        System.out.println("5. Exit");
    }
    private void printActiuniUtilizatori(){
        System.out.println("\t1. Adauga utilizator");
        System.out.println("\t2. Sterge utilizator");
        System.out.println("\t3. Modifica utilizator");
        System.out.println("\t4. Afiseaza prietenii unui utilizator");
        System.out.println("\t5. Afiseaza toti utilizatori");
        System.out.println("\t6. Afiseaza numar comunitati");
        System.out.println("\t7. Afiseaza cea mai sociabila comunitate");
        System.out.println("\t8. Afiseaza prietenii unui utilizator dintr-o anumita luna");
    }
    private void printActiuniPrietenii(){
        System.out.println("\t1. Adauga prietenie");
        System.out.println("\t2. Sterge prietenie");
        System.out.println("\t3. Afiseaza prietenii");
    }

    private void printActiuniMesaje(){
        System.out.println("\t1. Creare mesaj");
        System.out.println("\t2. Sterge mesaj");
        System.out.println("\t3. Raspunde la conversatie");
        System.out.println("\t4. Afisare conversatii");
    }

    private void printActiuniCereriDePrietenie(){
        System.out.println("\t1. Trimite cerere");
        System.out.println("\t2. Sterge cerere");
        System.out.println("\t3. Accepta cerere");
        System.out.println("\t4. Respinge cerere");
    }

    private void adaugaUtilizator() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("Dati prenume: ");
            String firstName = reader.readLine();
            System.out.print("Dati nume: ");
            String lastName = reader.readLine();
            Utilizator utilizator = new Utilizator(null, firstName, lastName,0);
            this.srvUtilizatori.addEntity(utilizator);
        }
        catch(RepositoryException | ValidationException  e) {
            System.out.println(e.getMessage());
        }
    }
    private void stergeUtilizator() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Dati id: ");
        try {
            Long id = Long.parseLong(reader.readLine());
            Utilizator utilizator = this.srvUtilizatori.getEntity(id);
            this.srvMesaje.deleteMesajeUtilizator(utilizator);
            this.srvCereriPrietenie.deleteCereri(utilizator);
            this.srvPrietenii.deletePrietenii(utilizator);
            this.srvUtilizatori.deleteEntity(utilizator);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch(RepositoryException e){
            System.out.println(e.getMessage());
        }
    }

    private void modificaUtilizator() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Dati id: ");
        try {
            Long id = Long.parseLong(reader.readLine());
            System.out.print("Dati nume nou: ");
            String lastName = reader.readLine();
            System.out.print("Dati prenume nou: ");
            String firstName = reader.readLine();
            Utilizator utilizator = new Utilizator(id, firstName,lastName,0);
            this.srvUtilizatori.updateEntity(utilizator);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch(RepositoryException | ValidationException e){
            System.out.println(e.getMessage());
        }
    }

    private void afiseazaPrieteniUtilizator() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Dati id: ");
        try {
            Long id = Long.parseLong(reader.readLine());
            Utilizator utilizator = this.srvUtilizatori.getEntity(id);
            if (utilizator.getFriends().isEmpty()) {
                System.out.println("Utilizatorul nu are prieteni!");
                return;
            }
            this.srvUtilizatori.getPrieteniUtilizator(utilizator).forEach(System.out::println);
        }
        catch(RepositoryException e){
            System.out.println(e.getMessage());
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
    }

    private void afiseazaUtilizatori(){
        if(this.srvUtilizatori.getAll().isEmpty()){
            System.out.println("Nu exista utilizatori!");
        }
        this.srvUtilizatori.getAll().forEach(x->{
            System.out.println(x);
        });
    }

    private void afiseazaPrieteniUtilizatorLuna() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Dati id: ");
        try {
            Long id = Long.parseLong(reader.readLine());
            System.out.print("Dati luna: ");
            Long luna = Long.parseLong(reader.readLine());
            Utilizator utilizator = this.srvUtilizatori.getEntity(id);
            if (utilizator.getFriends().isEmpty()) {
                System.out.println("Utilizatorul nu are prieteni!");
                return;
            }
            this.srvUtilizatori.getPrieteniUtilizatorDupaLuna(utilizator,luna).forEach(System.out::println);
        }
        catch(RepositoryException e){
            System.out.println(e.getMessage());
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural si luna cuprinsa intre 1 si 12!");
        }
    }

    private void adaugaPrietenie() throws IOException {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Dati id-ul primului utilizator: ");
            Long id1 = Long.parseLong(reader.readLine());
            System.out.print("Dati id-ul celui de-al doilea utilizator: ");
            Long id2 = Long.parseLong(reader.readLine());
            if (this.srvUtilizatori.getEntity(id1) == null || this.srvUtilizatori.getEntity(id2)==null) {
                System.out.println("Nu exista utilizatori cu aceste id-uri!");
                return;
            }
            Prietenie prietenie = new Prietenie(new Tuple<Long,Long>(id1,id2));
            this.srvPrietenii.addEntity(prietenie);
            Utilizator user1 = this.srvUtilizatori.getEntity(id1);
            Utilizator user2 = this.srvUtilizatori.getEntity(id2);
            this.srvUtilizatori.addPrieten(user1,user2);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch(RepositoryException | ValidationException e){
            System.out.println(e.getMessage());
        }
    }

    private void stergePrietenie() throws IOException {
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Dati id-ul primului utilizator: ");
            Long id1 = Long.parseLong(reader.readLine());
            System.out.print("Dati id-ul celui de-al doilea utilizator: ");
            Long id2 = Long.parseLong(reader.readLine());
            Prietenie prietenie = new Prietenie(new Tuple<Long,Long>(id1,id2));
            this.srvPrietenii.deleteEntity(prietenie);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch(RepositoryException e){
            System.out.println(e.getMessage());
        }
    }

    private void afiseazaPrietenii(){
        if(this.srvPrietenii.getAll().isEmpty()){
            System.out.println("Nu exista prietenii!");
        }
        this.srvPrietenii.getAll().forEach(x->{
            System.out.println(x);
        });
    }

    private void afiseazaComunnitati(){
        AtomicReference<Integer> nr = new AtomicReference<>(1);
        List<List<Long>> comunitati=this.srvUtilizatori.findCommunities();
        comunitati.forEach(lst->{
            System.out.println("Comunitatea numarul " + nr + ":");
            lst.forEach(userId->{
                System.out.println(this.srvUtilizatori.getEntity(userId));
            });
            nr.set(nr.get() + 1);
        });
    }

    private void afiseazaCeaMaiSociabilaComunitate(){
        List<Long> rez = this.srvUtilizatori.findMostSociableCommunity();
        rez.forEach(id->{
            Utilizator user = this.srvUtilizatori.getEntity(id);
            System.out.println(user);
        });
    }

    private void creareMesaj() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.print("Dati id-ul utilizatorului care trimite mesajul: ");
            Long idUtilizator = Long.parseLong(reader.readLine());
            this.srvUtilizatori.getEntity(idUtilizator);
            System.out.print("Dati continut mesaj: ");
            String continutMesaj = reader.readLine();
            System.out.print("Dati id-ul utilizatorilor carora vreti sa le trimiteti acest mesaj: ");
            String[] ids = reader.readLine().split(" ");
            List<Long> usrList = new ArrayList<>();
            Arrays.stream(ids)
                    .forEach(idUser->{
                        this.srvUtilizatori.getEntity(Long.parseLong(idUser));
                        usrList.add(Long.parseLong(idUser));
                    });
            Message msg = new Message(null,idUtilizator,continutMesaj,usrList);
            this.srvMesaje.addEntity(msg);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch (RepositoryException | ValidationException e){
            System.out.println(e.getMessage());
        }
    }

    private void stergeMesaj() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.print("Dati id mesaj: ");
            Long idMsg = Long.parseLong(reader.readLine());
            Message msg = this.srvMesaje.getEntity(idMsg);
            this.srvMesaje.deleteEntity(msg);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch (RepositoryException e){
            System.out.println(e.getMessage());
        }
    }

    public void raspundeLaConversatie() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.print("Dati id-ul mesajului caruia vreti sa-i faceti reply: ");
            Long idMsgReply = Long.parseLong(reader.readLine());
            this.srvMesaje.getEntity(idMsgReply);
            System.out.print("Dati id-ul utilizatorului care trimite acest minunat mesaj de reply: ");
            Long idCine = Long.parseLong(reader.readLine());
            this.srvUtilizatori.getEntity(idCine);
            System.out.print("Dati id-ul utilizatorului care primeste acest minunat mesaj de reply: ");
            String[] idsCui = reader.readLine().split(" ");
            List<Long> cui = new ArrayList<>();
            Arrays.stream(idsCui).forEach(idCui->{
                cui.add(Long.parseLong(idCui));
            });
            System.out.print("Dati mesaj: ");
            String text = reader.readLine();
            Message msgReply = new ReplyMessage(null,idCine,text,cui,idMsgReply);
            this.srvMesaje.addEntity(msgReply);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch (RepositoryException | ValidationException e){
            System.out.println(e.getMessage());
        }
    }

    private void afisareConversatie() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.print("Dati id utilizator1: ");
            Long idUsr1 = Long.parseLong(reader.readLine());
            System.out.print("Dati id utilizator2: ");
            Long idUsr2 = Long.parseLong(reader.readLine());
            Utilizator u1 = this.srvUtilizatori.getEntity(idUsr1);
            Utilizator u2 = this.srvUtilizatori.getEntity(idUsr2);
            this.srvMesaje.getSortedMesaje(u1,u2).
            forEach(System.out::println);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch (RepositoryException e){
            System.out.println(e.getMessage());
        }
    }

    private void adaugaCerereDePrietenie() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("Dati id-ul utilizatorului care trimite cererea: ");
            Long idFrom = Long.parseLong(reader.readLine());
            this.srvUtilizatori.getEntity(idFrom);
            System.out.print("Dati id-ul utilizatorului care primeste cererea: ");
            Long idTo = Long.parseLong(reader.readLine());
            this.srvUtilizatori.getEntity(idTo);
            CererePrietenie cererePrietenie = new CererePrietenie(null,idFrom,idTo);
            this.srvCereriPrietenie.addEntity(cererePrietenie);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch (RepositoryException | ValidationException e){
            System.out.println(e.getMessage());
        }
    }

    private void stergeCerereDePrietenie() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.print("Dati id cerere: ");
            Long idCerere = Long.parseLong(reader.readLine());
            CererePrietenie cererePrietenie = this.srvCereriPrietenie.getEntity(idCerere);
            this.srvCereriPrietenie.deleteEntity(cererePrietenie);
            Utilizator utilizator = this.srvUtilizatori.getEntity(cererePrietenie.getTo());
            utilizator.removeCerere(cererePrietenie);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch (RepositoryException | ValidationException e){
            System.out.println(e.getMessage());
        }
    }

    private void acceptaCerereDePrietenie() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.print("Dati id utilizator: ");
            Long idUtilizator = Long.parseLong(reader.readLine());
            Utilizator utilizator = this.srvUtilizatori.getEntity(idUtilizator);
            if(utilizator.getCereri().stream().
                    filter(cererePrietenie -> cererePrietenie.getStatus().equals("PENDING")).count() == 0){
                        System.out.println("Utilizatorul dat nu are cereri de prietenie!");
                        return;
            }
            utilizator.getCereri().stream()
                .filter(cererePrietenie -> cererePrietenie.getStatus().equals("PENDING"))
                .forEach(System.out::println);
            ;
            System.out.print("Dati id-ul cererilor pe care vreti sa le acceptati: ");
            List<Long> idCereri = Arrays.stream(reader.readLine().split(" "))
                                    .map(sCerere->Long.parseLong(sCerere))
                                    .collect(Collectors.toList());
            this.srvCereriPrietenie.acceptaCerere(idCereri);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch (RepositoryException | ValidationException e){
            System.out.println(e.getMessage());
        }
    }

    private void respingeCerere() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.print("Dati id utilizator: ");
            Long idUtilizator = Long.parseLong(reader.readLine());
            Utilizator utilizator = this.srvUtilizatori.getEntity(idUtilizator);
            if(utilizator.getCereri().stream().
                    filter(cererePrietenie -> cererePrietenie.getStatus().equals("PENDING")).count() == 0){
                        System.out.println("Utilizatorul dat nu are cereri de prietenie!");
                        return;
            }
            utilizator.getCereri().stream()
                    .filter(cererePrietenie -> cererePrietenie.getStatus().equals("PENDING"))
                    .forEach(System.out::println);
            ;
            System.out.print("Dati id-ul cererilor pe care vreti sa le respingeti: ");
            List<Long> idCereri = Arrays.stream(reader.readLine().split(" "))
                    .map(sCerere->Long.parseLong(sCerere))
                    .collect(Collectors.toList());
            this.srvCereriPrietenie.respingeCerere(idCereri);
        }
        catch (NumberFormatException e){
            System.out.println("Id-ul trebuie sa fie numar natural!");
        }
        catch (RepositoryException | ValidationException e){
            System.out.println(e.getMessage());
        }
    }

    public void run() throws IOException {
        boolean isRunning = true;
        while (isRunning) {
            this.printMenu();
            System.out.print("Dati comanda: ");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String command = reader.readLine();
            switch (command) {
                case "1":
                    this.printActiuniUtilizatori();
                    System.out.print("Dati comanda: ");
                    String command2 = reader.readLine();
                    switch (command2){
                        case"1":
                            this.adaugaUtilizator();
                            break;
                        case"2":
                            this.stergeUtilizator();
                            break;
                        case"3":
                            this.modificaUtilizator();
                            break;
                        case"4":
                            this.afiseazaPrieteniUtilizator();
                            break;
                        case"5":
                            this.afiseazaUtilizatori();
                            break;
                        case"6":
                            this.afiseazaComunnitati();
                            break;
                        case"7":
                            this.afiseazaCeaMaiSociabilaComunitate();
                            break;
                        case "8":
                            this.afiseazaPrieteniUtilizatorLuna();
                            break;
                        default:
                            System.out.println("Comanda invalida!");
                    }
                    break;
                case "2":
                    this.printActiuniPrietenii();
                    System.out.print("Dati comanda: ");
                    command2 = reader.readLine();
                    switch (command2){
                        case"1":
                            this.adaugaPrietenie();
                            break;
                        case"2":
                            this.stergePrietenie();
                            break;
                        case"3":
                            this.afiseazaPrietenii();
                            break;
                        default:
                            System.out.println("Comanda invalida!");
                    }
                    break;
                case"3":
                    this.printActiuniMesaje();
                    System.out.print("Dati comanda: ");
                    command2 = reader.readLine();
                    switch (command2){
                        case "1":
                            this.creareMesaj();
                            break;
                        case "2":
                            this.stergeMesaj();
                            break;
                        case "3":
                            this.raspundeLaConversatie();
                            break;
                        case "4":
                            this.afisareConversatie();
                            break;
                        default:
                            System.out.println("Comanda invalida!");
                    }
                    break;
                case "4":
                    this.printActiuniCereriDePrietenie();
                    System.out.print("Dati comanda: ");
                    command2 = reader.readLine();
                    switch (command2){
                        case "1":
                            this.adaugaCerereDePrietenie();
                            break;
                        case "2":
                            this.stergeCerereDePrietenie();
                            break;
                        case "3":
                            this.acceptaCerereDePrietenie();
                            break;
                        case "4":
                            this.respingeCerere();
                            break;
                        default:
                            System.out.println("Comanda invalida!");
                    }
                    break;
                case "5":
                    System.out.println("SAIONARA!");
                    isRunning=false;
                    break;
                default:
                    System.out.println("Comanda invalida!");
            }
        }
    }
}
